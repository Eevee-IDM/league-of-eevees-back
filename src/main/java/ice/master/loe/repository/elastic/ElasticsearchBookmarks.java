package ice.master.loe.repository.elastic;

import java.io.IOException;
import java.util.Optional;
import java.util.function.Function;

import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestResult;
import io.searchbox.core.Delete;
import io.searchbox.core.Get;
import io.searchbox.core.Index;
import io.searchbox.core.Search;
import io.searchbox.indices.Refresh;

/**
 * Provides a way to query users' favorites that are stored in the database.
 * 
 * @author François-Marie d'Aboville
 * @author Emmanuel Chebbi
 */
public class ElasticsearchBookmarks {

	private static final String REQUEST_INDEX = "request";
	private static final String REQUEST_TYPE = "data";

	private static JestClient client = ElasticsearchManager.client;

	/**
	 * Returns all the favorites of a given summoner.
	 * 
	 * @param accountId
	 *            The id of summoner's account
	 *            
	 * @return all the favorites of given summoner
	 * 
	 * @throws IOException if an error occurs while querying the database 
	 */
	public JsonArray getAllBookmarks(String accountId) throws IOException {
		SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
		String query = searchSourceBuilder.query(QueryBuilders.matchQuery("accountId", accountId)).toString();
		Search search = new Search.Builder(query).addIndex(REQUEST_INDEX).addType(REQUEST_TYPE).build();
		
		JestResult result = client.execute(search);
		return ElasticsearchManager.getSourceAsJsonArray(result);
	}
	
	/**
	 * Returns a specific bookmark, if it can be found.
	 * 
	 * @param id
	 * 			The id of the bookmark to retrieve.
	 * 
	 * @return the bookmark identified by {@code id}, if it can be found
	 * 
	 * @throws IOException if an error occurs while querying the database
	 */
	public Optional<JsonObject> getBookmark(String id) throws IOException {
		JestResult result = null;
		Get get = new Get.Builder(REQUEST_INDEX, id).type(REQUEST_TYPE).build();
		result = client.execute(get);
		
		if (! result.isSucceeded())
			return Optional.empty();
		
		return Optional.of(ElasticsearchManager.getSourceAsJsonObject(result))
					   .map(withId(id));
	}
	
	private Function<JsonObject, JsonObject> withId(String id) {
		return bookmark -> {
			bookmark.addProperty("id", id);
			return bookmark;
		};
	}

	/**
	 * Creates a new bookmark in the database.<br>
	 * <br>
	 * If a bookmark with the given id already exist, it is replaced.
	 * 
	 * @param id
	 * 			The id of the bookmark to create.
	 * @param request
	 * 			The details of the bookmark.
	 * 
	 * @throws IOException if an error occurs while querying the database
	 */
	public void createBookmark(String id, JsonObject request) throws IOException {
		Index index = new Index.Builder(request).index(REQUEST_INDEX).type(REQUEST_TYPE).id(id).build();
		client.execute(index);
		Refresh refresh = new Refresh.Builder().addIndex(REQUEST_INDEX).build();
		client.execute(refresh);
	}

	/**
	 * Removes a bookmark from the database.
	 * 
	 * @param id
	 * 			The id of the bookmark to remove.
	 * 
	 * @return whether a bookmark has been deleted
	 * 
	 * @throws IOException if an error occurs while querying the database
	 */
	public boolean deleteBookmark(String id) throws IOException {
		JestResult result = client.execute(new Delete.Builder(id).index(REQUEST_INDEX).type(REQUEST_TYPE).build());
		Refresh refresh = new Refresh.Builder().addIndex(REQUEST_INDEX).build();
		client.execute(refresh);
		return result.isSucceeded();
	}
}
