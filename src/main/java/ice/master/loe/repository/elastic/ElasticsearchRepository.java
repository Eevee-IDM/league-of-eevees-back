package ice.master.loe.repository.elastic;

import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import ice.master.loe.repository.RepositoryInterface;
import io.searchbox.client.JestClient;
import io.searchbox.client.JestResult;
import io.searchbox.core.Bulk;
import io.searchbox.core.Bulk.Builder;
import io.searchbox.core.Get;
import io.searchbox.core.Index;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import io.searchbox.indices.IndicesExists;
import io.searchbox.indices.Refresh;
import io.searchbox.params.Parameters;

/**
 * Provides an easy way to get and set the content of an Elasticsearch database.<br>
 * Internally relies on {@link ElasticsearchManager}.
 * 
 * @author François-Marie d'Aboville
 */
public class ElasticsearchRepository implements RepositoryInterface {

	private static final Logger LOGGER = LogManager.getLogger(ElasticsearchRepository.class.getName());

	/** The default size for a search in the database */
	private static final int DEFAULT_SEARCH_SIZE = 100;

	// The identifiers used in Elasticsearch database
	private static final String MATCHES = "matches";
	private static final String MATCHLIST = "matchlist";
	private static final String TIMELINE = "timelines";
	private static final String GAME = "game";

	private static final JestClient CLIENT = ElasticsearchManager.client;

	/**
	 * Persists the historic of a given player.
	 * 
	 * @param historic
	 * 			The whole list of games played by the player.
	 * @param accountId
	 * 			The identifier of the player's account.
	 * 
	 * @throws IOException if an error occurs while processing
	 */
	public void setHistoric(JsonObject historic, String accountId) throws IOException {
		if (historic.has(MATCHES)) {
			JsonArray matches = historic.getAsJsonArray(MATCHES);
			Builder bulk = new Bulk.Builder().defaultIndex(accountId).defaultType(MATCHLIST);
			for (JsonElement match : matches) {
				bulk.addAction(new Index.Builder(match.getAsJsonObject())
						.id(match.getAsJsonObject().get("gameId").getAsString()).build());
			}
			CLIENT.execute(bulk.build());
		}
		Refresh refresh = new Refresh.Builder().addIndex(accountId).build();
		CLIENT.execute(refresh);
	}

	/**
	 * Persists the statistics of a game into Elasticsearch.
	 * 
	 * @param match
	 * 			The game which statistics have to be persisted.
	 * 
	 * @throws IOException if an error occurs while querying the data
	 */
	public void setGameStats(JsonObject match) throws IOException {
		Index index = new Index.Builder(match).index(MATCHES).type(GAME).id(match.get("gameId").getAsString()).build();
		CLIENT.execute(index);
		Refresh refresh = new Refresh.Builder().addIndex(MATCHES).build();
		CLIENT.execute(refresh);
	}

	/**
	 * Persists the timeline of a game into Elasticsearch.
	 * 
	 * @param match
	 * 			The game which timeline has to be persisted.
	 * @param gameId
	 * 			The identifier of the game.
	 * 
	 * @throws IOException if an error occurs while querying the data
	 */
	public void setGameTimeline(JsonObject match, String gameId) throws IOException {
		Index index = new Index.Builder(match).index(TIMELINE).type(GAME).id(gameId).build();
		CLIENT.execute(index);
		Refresh refresh = new Refresh.Builder().addIndex(TIMELINE).build();
		CLIENT.execute(refresh);
	}

	/**
	 * Checks whether an historic has been persisted for a given player.
	 * 
	 * @param accountId
	 * 			The identifier of the account to look up.
	 * 
	 * @return {@code true} if an historic has been found, {@code false} otherwise
	 * 
	 * @throws IOException if an error occurs while querying the data
	 */
	public boolean hasHistoricOf(String accountId) throws IOException {
		JestResult exists = CLIENT.execute(new IndicesExists.Builder(accountId).build());
		return exists.isSucceeded();
	}

	/**
	 * Checks whether the statistics of a given game have been stored.
	 * 
	 * @param gameId
	 * 			The identifier of the game to check.
	 * 
	 * @return {@code true} if the statistics have been found, {@code false} otherwise
	 * 
	 * @throws IOException if an error occurs while querying Elasticsearch database
	 */
	public boolean hasStatisticsOf(String gameId) throws IOException {
		JestResult game = this.getGame(MATCHES, gameId);

		if (game == null || !game.isSucceeded())
			return false;

		return (boolean) game.getValue("found");
	}

	/**
	 * Checks whether the timeline of a given game have been stored.
	 * 
	 * @param gameId
	 * 			The identifier of the game to check.
	 * 
	 * @return {@code true} if the timeline have been found, {@code false} otherwise
	 * 
	 * @throws IOException if an error occurs while querying Elasticsearch database 
	 */
	public boolean hasTimelineOf(String gameId) throws IOException {
		JestResult game = this.getGame(TIMELINE, gameId);

		if (game == null || !game.isSucceeded())
			return false;

		return (boolean) game.getValue("found");
	}

	@Override
	public JsonObject getHistoric(String accountId, Long beginTime, Long endTime, int beginIndex, int endIndex) throws IOException {
		// Initialize objects
		JsonObject json = new JsonObject();
		JsonParser parser = new JsonParser();
		io.searchbox.core.Search.Builder search = null;
		Search buildedSearch = null;
		SearchResult result = null;

		// Build query
		SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
		BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();

		// Perform time entry
		if (beginTime != null && endTime != null) {
			boolQuery = QueryBuilders.boolQuery().must(boolQuery);
			RangeQueryBuilder rangeQuery = QueryBuilders.rangeQuery("timestamp");
			rangeQuery.from(beginTime).to(endTime);
			boolQuery.must(rangeQuery);
		}

		// Sort games chronologically
		String query = searchSourceBuilder.query(boolQuery).sort("timestamp", SortOrder.DESC).toString();
		LOGGER.debug("Asking Elasticsearch for an historic of account: " + accountId + " => " + query);

		// Build Search
		if (accountId != null) {
			search = new Search.Builder(query).addIndex(accountId).addType(MATCHLIST);

			if (endIndex != -1) {
				int size = endIndex;
				if (beginIndex != -1) {
					search.setParameter(Parameters.FROM, beginIndex);
					size = endIndex - beginIndex;
				}
				search.setParameter(Parameters.SIZE, size);
			} else {
				search.setParameter(Parameters.SIZE, DEFAULT_SEARCH_SIZE);
			}
			buildedSearch = search.build();
		}
		// Execute Search
		result = CLIENT.execute(buildedSearch);

		// Parse result
		json.add(MATCHES, parser.parse("[" + result.getSourceAsString() + "]"));
		json.addProperty("totalGames", result.getTotal());
		return json;
	}

	/**
	 * Retrieves all the games played by a given player in a given pagination.
	 * 
	 * @param accountId
	 * 			The identifier of the player's account.
	 * @param beginIndex
	 * 			The first index of the pagination in Elasticsearch.
	 * @param endIndex
	 * 			The last index of the pagination in Elasticsearch.
	 * 
	 * @return all the games played by a given player in a given pagination
	 * 
	 * @throws IOException if an error occurs while querying the data
	 */
	public JsonObject getHistoric(String accountId, int beginIndex, int endIndex) throws IOException {
		return this.getHistoric(accountId, null, null, beginIndex, endIndex);
	}

	@Override
	public JsonObject getGameStats(String gameId) throws IOException {
		JestResult game = this.getGame(MATCHES, gameId);

		if (game == null || !game.isSucceeded())
			return null;

		return ElasticsearchManager.getSourceAsJsonObject(game);
	}

	@Override
	public JsonObject getGameTimeline(String gameId) throws IOException {
		JestResult game = this.getGame(TIMELINE, gameId);

		if (game == null || !game.isSucceeded())
			return null;

		return ElasticsearchManager.getSourceAsJsonObject(game);
	}
	
	/**
	 * Retrieves the statistics of a given set of games.
	 * 
	 * @param gameIds
	 * 			The identifiers of the games to retrieve.
	 * 
	 * @return an array containing all the statistics, or {@code null} if an error occurs
	 * 
	 * @throws IOException if an error occurs while querying Elasticsearch database
	 */
	public JsonArray getGamesStats(List<String> gameIds) throws IOException {
		JestResult games = this.getGames(MATCHES, gameIds);

		if (games == null || !games.isSucceeded())
			return null;
		
		return ElasticsearchManager.getSourceAsJsonArray(games);
	}

	/**
	 * Retrieves all the information about a given game.
	 * 
	 * @param index
	 * 			The index to query in the database.
	 * @param gameId
	 * 			The identifier of the game.
	 * 
	 * @return all the information of the game, or {@code null} if it cannot be found
	 * 
	 * @throws IOException if an error occurs while querying Elasticsearch database
	 */
	private JestResult getGame(String index, String gameId) throws IOException {
		Get get = new Get.Builder(index, gameId).type(GAME).build();
		return CLIENT.execute(get);
	}

	/**
	 * Retrieves the information about many games.
	 * 
	 * @param index
	 * 			The index to query in the database.
	 * @param gameIds
	 * 			The identifiers of the games to find.
	 * 
	 * @return all the information about a set of game, or {@code null} if it cannot be found
	 * 
	 * @throws IOException if an error occurs while querying Elasticsearch database
	 */
	private JestResult getGames(String index, List<String> gameIds) throws IOException {
		SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
		BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();

		// Add different should queries to match
		for (String id : gameIds) {
			boolQuery.should(QueryBuilders.matchQuery("_id", id));
		}
		String query = searchSourceBuilder.query(boolQuery).toString();
		io.searchbox.core.Search.Builder search = new Search.Builder(query).addIndex(index).addType(GAME);
		Search builded = search.setParameter(Parameters.SIZE, gameIds.size()).build();

		return CLIENT.execute(builded);
	}
}
