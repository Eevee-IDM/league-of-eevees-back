/**
 * 
 */
package ice.master.loe.repository.elastic;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.JestResult;
import io.searchbox.client.config.HttpClientConfig;

/**
 * Initialize the static connection to Elasticsearch, to be used in several
 * repositories
 * 
 * @author fmdaboville
 *
 */
class ElasticsearchManager {
	
	/**
	 * {@link JestClient} to init and use
	 */
	public static final JestClient client;

	static {
		// Because Jest is design to be a singleton
		JestClientFactory factory = new JestClientFactory();
		
		String elasticIp = System.getenv("LOE_ELASTIC_URL");
		
		if(elasticIp == null)
			elasticIp = "ip90.ip-137-74-154.eu:9200";
		
		factory.setHttpClientConfig(new HttpClientConfig.Builder("http://" + elasticIp)
				.multiThreaded(true).readTimeout(60000).connTimeout(60000).build());
		client = factory.getObject();
	}
	
	private ElasticsearchManager() {
		// should not be instantiated
	}
	
	/**
	 * Retrieve only the source from a result
	 * 
	 * @param source
	 * @return source as string
	 */
	public static JsonObject getSourceAsJsonObject(JestResult source) {
		JsonParser parser = new JsonParser();
		return (JsonObject) parser.parse(source.getSourceAsString());
	}
	
	/**
	 * Retrieve only the source from a result to a json array
	 * 
	 * @param source
	 * @return source as string
	 */
	public static JsonArray getSourceAsJsonArray(JestResult source) {
		JsonParser parser = new JsonParser();
		return (JsonArray) parser.parse("[" + source.getSourceAsString() + "]");
	}
}
