package ice.master.loe.repository;

import java.io.IOException;

import com.google.gson.JsonObject;

public interface RepositoryInterface {

	/**
	 * Returns all the games played by a given player.
	 * 
	 * @param accountId
	 *            The id of the player account to retrieve games
	 * @param beginTime
	 *            Date to start search
	 * @param endTime
	 *            Date to stop search
	 * @param beginIndex
	 *            Begin of the pagination
	 * @param endIndex
	 *            End of the pagination
	 *            
	 * @return {@link JsonObject} representing the historic
	 * 
	 * @throws IOException if an error occurs while querying the data
	 */
	public JsonObject getHistoric(String accountId, Long beginTime, Long endTime, int beginIndex, int endIndex) throws IOException;

	/**
	 * Returns the statistics of a specific game.
	 * 
	 * @param gameId
	 *            The id of the game. Must not be {@code null}.
	 *            
	 * @return the statistics of the game
	 * 
	 * @throws IOException if an error occurs while querying the data
	 */
	public JsonObject getGameStats(String gameId) throws IOException;

	/**
	 * Returns the whole timeline of a specific game.<br>
	 * <br>
	 * The timeline contains all the events that occurred during the game.
	 * 
	 * @param gameId
	 *            The id of the game. Must not be {@code null}.
	 *            
	 * @return the timeline of the game
	 * 
	 * @throws IOException if an error occurs while querying the data
	 */
	public JsonObject getGameTimeline(String gameId) throws IOException;

}
