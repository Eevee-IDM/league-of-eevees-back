package ice.master.loe.repository.rito;

import org.apache.http.HttpResponse;

/**
 * The response of an HTTP request sent to Riot.
 * 
 * @author Léo Calvis
 */
class RitoResponse {
	
	private final HttpResponse response;
	private final String body;
	
	RitoResponse(HttpResponse response, String body) {
		this.response = response;
		this.body = body;
	}
	
	HttpResponse getResponse() {
		return this.response;
	}
	
	String getBody() {
		return this.body;
	}

}
