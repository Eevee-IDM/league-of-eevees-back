package ice.master.loe.repository.rito;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Manages HTTP requests sent to Riot Games.
 * 
 * @author Léo Calvis
 */
public enum RitoRequestsManager {

	INSTANCE;

	private static final Logger LOGGER = LogManager.getLogger(RitoRequestsManager.class.getName());

	private static final int LIMIT_SECOND = 20;
	private static final int LIMIT_2_MINUTES = 100;

	private final String ritoApiKey;
	
	private final BlockingQueue<String> queueRequests;
	private final BlockingQueue<String> queueResponses;
	private final Thread requestsThread;

	private RitoRequestsManager() {
		
		if(System.getenv("RIOT_API_KEY") != null)
			ritoApiKey = System.getenv("RIOT_API_KEY");
		else
			ritoApiKey = "RGAPI-ad36ed7b-c369-414d-9db4-e3e13dcacbdd";
		
		requestsThread = new Thread(new RequestsThread());
		queueRequests = new LinkedBlockingQueue<>();
		queueResponses = new LinkedBlockingQueue<>();

		requestsThread.start();
	}

	/**
	 * Processes requests pushed in {@code queueRequests}.
	 */
	private class RequestsThread implements Runnable {

		private static final String RATE_LIMIT_EXCEEDED_STATUS_CODE = "\"status_code\":429";
		private final SortedSet<Long> requestsTimeSecond = new TreeSet<>();
		private final SortedSet<Long> requestsTime2Minutes = new TreeSet<>();
		
		private boolean dontStop = true;

		@Override
		public void run() {
			while (dontStop) {
				try {
					String request = queueRequests.take();
					String body = "";
					boolean retry = true;
					
					while (retry) {
						retry = false;
						
						waitForLimit();
						
						String address = "https://euw1.api.riotgames.com" + request + "api_key=" + ritoApiKey;
						address = address.replaceAll(" ", "%20");
						LOGGER.info("Sending " + address);
						RitoResponse req = get(address);
	
						long time = Timestamp.from(Instant.now()).getTime();
						requestsTimeSecond.add(time);
						requestsTime2Minutes.add(time);
						
						body = req.getBody();
						LOGGER.info("Riot response received : " + body.substring(0, body.length() < 100 ? body.length()-1 : 100) + "...");
						
						if (body.contains(RATE_LIMIT_EXCEEDED_STATUS_CODE)) {
							retry = true;
							fillQueueRequest(time);
						}
					}
					
					queueResponses.put(body);

				} catch (InterruptedException e) {
					LOGGER.error("Current thread has been interrupted while sending requests to Riot API", e);
					Thread.currentThread().interrupt();
				} catch (Exception e) {
					LOGGER.error("An unexpected error occurred while sending requests to Riot API", e);
					
					putError();
				}
			}
		}

		private void putError() {
			boolean retry = false;
			while(!retry)
				retry = queueResponses.offer("{\"error\":true}");
		}

		private void fillQueueRequest(long time) {
			while (requestsTime2Minutes.size() < LIMIT_2_MINUTES)
				requestsTime2Minutes.add(time++);
		}

		private RitoResponse get(String url) throws IOException, URISyntaxException {
			RitoResponse result = new RitoResponse(null, "{\"error\":true}");
			
			int timeout = 5000;
            
            RequestConfig config = RequestConfig.custom()
            		  .setConnectTimeout(timeout)
            		  .setConnectionRequestTimeout(timeout)
            		  .setSocketTimeout(timeout).build();
			
	        try (CloseableHttpClient httpclient = HttpClientBuilder.create().setDefaultRequestConfig(config).build()) {
	            
	        	boolean retry = true;
	            int nb = 0;
	            while(retry)
	            try {
		        	HttpGet httpget = new HttpGet(new URI(url));
	
		            ResponseHandler<RitoResponse> responseHandler = new ResponseHandler<RitoResponse>() {
	
		                @Override
		                public RitoResponse handleResponse(final HttpResponse response) throws IOException {
		                        HttpEntity entity = response.getEntity();
		                        return entity != null ? new RitoResponse(response, EntityUtils.toString(entity)) : null;
		                	}
		            };
	            
	            
	            	retry = false;
	            	nb++;
	            	RitoResponse response = httpclient.execute(httpget, responseHandler);
		            result = response;
	            }catch(ConnectTimeoutException | java.net.SocketTimeoutException e) {
	            	LOGGER.error("Request rito timeout");
	            	if(nb < 4)
	            		retry = true;
	            		
	            }
	            
	        }
	        
			return result;
		}

		private void waitForLimit() {
			long now = Timestamp.from(Instant.now()).getTime();
			while (!requestsTime2Minutes.isEmpty()) {
				long time = requestsTime2Minutes.first();
				if (now - time > 120 * 1000)
					requestsTime2Minutes.remove(time);
				else
					break;
			}
			if (requestsTime2Minutes.size() >= LIMIT_2_MINUTES) {
				try {
					LOGGER.info("Rito limit 2 minutes reached, wait " + (requestsTime2Minutes.first() + 120 * 1000 + 1000 - now) + " ms");
					Thread.sleep(requestsTime2Minutes.first() + 120 * 1000 + 10 - now);
					waitForLimit();
				} catch (InterruptedException e) {
					LOGGER.error("Current thread was interrupted while waiting for Riot's limit to be reach", e);
					Thread.currentThread().interrupt();
				}
				return;
			}

			while (!requestsTimeSecond.isEmpty()) {
				long time = requestsTimeSecond.first();
				if (now - time <= 1 * 1000)
					break;
				requestsTimeSecond.remove(time);
			}

			if (requestsTimeSecond.size() >= LIMIT_SECOND) {
				try {
					LOGGER.info("Rito limit 1 second reached, wait " + (requestsTimeSecond.first() + 1 * 1000 + 10 - now) + " ms");
					Thread.sleep(requestsTimeSecond.first() + 1 * 1000 + 10 - now);
					waitForLimit();
				} catch (InterruptedException e) {
					LOGGER.error("Current thread was interrupted while waiting for Riot's limit to be reach", e);
					Thread.currentThread().interrupt();
				}
			}
		}
	}

	/**
	 * Sends a new request to Riot.
	 * 
	 * @param req
	 * 			The request to send. Must not be {@code null}.
	 * 
	 * @return Riot response, as a JSON
	 */
	public String request(String req) {
		try {
			synchronized (this) {
				queueRequests.put(req);
				LOGGER.info("Waiting for Riot response ....");
				return queueResponses.take();
			}
		} catch (InterruptedException e) {
			LOGGER.error("The thread was interrupted while waiting for Riot response", e);
			Thread.currentThread().interrupt();
			return null;
		}
	}

}
