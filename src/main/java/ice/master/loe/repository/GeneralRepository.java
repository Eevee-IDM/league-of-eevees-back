package ice.master.loe.repository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import ice.master.loe.repository.elastic.ElasticsearchRepository;
import ice.master.loe.repository.rito.RitoRequestsManager;
import ice.master.loe.rest.exceptions.GameNotFoundException;

public class GeneralRepository implements RepositoryInterface{
	
	private static final String STATUS = "status";

	public static final GeneralRepository REPO = new GeneralRepository();
	
	
	private static final Logger LOGGER = LogManager.getLogger(GeneralRepository.class.getName());
	
	private static final JsonParser JPARSER = new JsonParser();
	
	private static final String MATCHES = "matches";
	private static final String TOTALGAMES = "totalGames";
	private static final String ENDINDEX = "endIndex";
	
	private ElasticsearchRepository elasticR;
	
	private GeneralRepository() {
		this.elasticR = new ElasticsearchRepository();
	}
	
	public Optional<String> getAccountId(String summonerName) {
		LOGGER.info(summonerName);
		JsonObject account = JPARSER.parse(RitoRequestsManager.INSTANCE.request("/lol/summoner/v3/summoners/by-name/" + summonerName + "?")).getAsJsonObject();
		if (! account.has("accountId"))
			return Optional.empty();
			
		return Optional.of(account.get("accountId").getAsString());
	}
	
	@Override
	public JsonObject getHistoric(String accountId, Long beginTime, Long endTime, int beginIndex, int endIndex) throws IOException {
		if(elasticR.hasHistoricOf(accountId)) {
			boolean hasLast = false;
			
			int begin = 0;
			int end = 100;
			
			long timeStampLastGameOnElastic = elasticR.getHistoric(accountId, begin, end).getAsJsonArray(MATCHES).get(0).getAsJsonObject().get("timestamp").getAsLong();
			
			while(!hasLast) {
				JsonObject jsonRito = getRitoHistoric(accountId, null, null, begin, end);
				JsonArray jsonRitoMatches = jsonRito.getAsJsonArray(MATCHES);
				
				if(jsonRitoMatches.size() == 0)
					break;
				
				hasLast = timeStampLastGameOnElastic >= jsonRitoMatches.get(jsonRitoMatches.size()-1).getAsJsonObject().get("timestamp").getAsLong();
				
				begin += 100;
				end += 100;
			}
			
		} else {
			getRitoHistoric(accountId, null, null, 0, Integer.MAX_VALUE);
		}
		
		return elasticR.getHistoric(accountId, beginTime, endTime, beginIndex, endIndex);
	}
	
	public JsonObject getRitoHistoric(String accountId, Long beginTime, Long endTime, int beginIndex, int endIndex) throws IOException {
		StringBuilder address = new StringBuilder("/lol/match/v3/matchlists/by-account/" + accountId + "?");
		JsonObject jsonResult = JPARSER.parse("{\"matches\":[],\"startIndex\":" + beginIndex + ",\"endIndex\":0,\"totalGames\":0}").getAsJsonObject();
		
		if (endTime == null)
			return getGamesRitoHistoric(address.toString(), accountId, beginIndex, endIndex < 0 ? 100 : endIndex);
		
		long beginT;
		long endT = endTime;
		int beginI = beginIndex;
		int endI = endIndex < 0 ? Integer.MAX_VALUE : endIndex;
		
		do {
			beginT = endT - 604_000_000;
			
			if (beginT < beginTime)
				beginT = beginTime;
			
			JsonObject jsonRequestResult = getGamesRitoHistoric(address + "beginTime=" + beginT + "&endTime=" + endT + "&", accountId, beginI, endI);
			
			if (! jsonRequestResult.has(STATUS)) {
				JsonArray matchesRequest = jsonRequestResult.get(MATCHES).getAsJsonArray();
				JsonArray matchesResult = jsonResult.get(MATCHES).getAsJsonArray();
				
				matchesResult.addAll(matchesRequest);
				
				jsonResult.addProperty(TOTALGAMES, jsonResult.get(TOTALGAMES).getAsInt() + jsonRequestResult.get(TOTALGAMES).getAsInt());
				jsonResult.addProperty(ENDINDEX, jsonResult.get(ENDINDEX).getAsInt() + jsonRequestResult.get(ENDINDEX).getAsInt());
				
				endI = endIndex - matchesResult.size() + (beginI - beginIndex);
				beginI -= jsonRequestResult.get(TOTALGAMES).getAsInt();
				
				if(beginI < 0)
					beginI = 0;
				
				if (matchesResult.size() >= endIndex - beginIndex)
					break;
			}
			endT = beginT;
			
		} while (beginT != beginTime);
		
		return jsonResult;
		
	}
	
	private JsonObject getGamesRitoHistoric(String address, String accountId, int beginIndex, int endIndex) throws IOException {
		int beginT = beginIndex;
		int endT;
		
		JsonObject jsonResult = JPARSER.parse("{\"matches\":[],\"startIndex\":" + beginIndex + ",\"endIndex\":0,\"totalGames\":0}").getAsJsonObject();
		
		do {
			endT = beginT + 100;
			
			if(endT > endIndex)
				endT = endIndex;
			
			JsonObject jsonRequestResult = JPARSER.parse(RitoRequestsManager.INSTANCE.request(address + "beginIndex=" + beginT + "&endIndex=" + endT + "&")).getAsJsonObject();
			
			if (! jsonRequestResult.has(STATUS)) {
				JsonArray matchesRequest = jsonRequestResult.get(MATCHES).getAsJsonArray();
				
				jsonResult.get(MATCHES).getAsJsonArray().addAll(matchesRequest);
				
				jsonResult.add(TOTALGAMES, jsonRequestResult.get(TOTALGAMES));
				jsonResult.add(ENDINDEX, jsonRequestResult.get(ENDINDEX));
				
				elasticR.setHistoric(jsonRequestResult, accountId);
				
				if (matchesRequest.size() == 0 || jsonRequestResult.get(TOTALGAMES).getAsInt() == jsonRequestResult.get(ENDINDEX).getAsInt())
					break;
			}
			
			beginT = endT;
			
		} while (endT != endIndex);
		
		return jsonResult;
	}

	@Override
	public JsonObject getGameStats(String gameId) {
		try {
			if (elasticR.hasStatisticsOf(gameId))
				return elasticR.getGameStats(gameId);
			
		} catch (IOException e) {
			LOGGER.error("Unable to retrieve the game (id: " + gameId + ") from Elasticsearch", e);
		}
		
		JsonObject riotResponse = JPARSER.parse(RitoRequestsManager.INSTANCE.request("/lol/match/v3/matches/" + gameId + "?")).getAsJsonObject();
		
		if (isError(riotResponse))
			throw new GameNotFoundException("Unable to find game (id: " + gameId + ") in Riot database");
		
		try {
			elasticR.setGameStats(riotResponse);
		} catch (IOException e) {
			LOGGER.error("An error occured while persisting statistics of game with id: " + gameId,  e);
		}
		
		return riotResponse;
	}
	
	/**
	 * Returns whether {@code response} corresponds to an error message.
	 * 
	 * @param response
	 * 			The response sent by Riot Games.
	 * 
	 * @return whether {@code response} corresponds to an error message
	 */
	private boolean isError(JsonObject response) {
		if(response.has(STATUS))
			return response.get(STATUS).getAsJsonObject().get("status_code").getAsInt() != 200;
		return false;
	}
	
	public List<JsonObject> getGamesStats(List<String> gameIds) {
		List<JsonObject> gamesStatistics = new ArrayList<>();
		
		for (int i = 0; i < gameIds.size(); i+=1000) {
			try {
				JsonArray jsonSubResult = elasticR.getGamesStats(gameIds.subList(i, Math.min(gameIds.size(), i+1000)));
				
				
				if(jsonSubResult != null)
					for (JsonElement e : jsonSubResult)
						gamesStatistics.add(e.getAsJsonObject());
				
			} catch (IOException e) {
				LOGGER.error("An error occured while retrieving the games (ids: " + gameIds + ") from Elasticseach", e);
			}
		}

		if (gameIds.size() != gamesStatistics.size()) {
			List<String> gamesPresent = new ArrayList<>();
			for (JsonObject e : gamesStatistics) {
				gamesPresent.add(e.get("gameId").getAsString());
			}
			
			for (String gameId : gameIds) {
				gamesStatistics.add(getGameStats(gameId));
			}
		}
		
		return gamesStatistics;
	}

	@Override
	public JsonObject getGameTimeline(String gameId) throws IOException {
		if (elasticR.hasTimelineOf(gameId))
			return elasticR.getGameTimeline(gameId);

		JsonObject riotResponse = JPARSER.parse(RitoRequestsManager.INSTANCE.request("/lol/match/v3/timelines/by-match/" + gameId + "?")).getAsJsonObject();
		
		if (isError(riotResponse))
			throw new GameNotFoundException("Unable to find game (id: " + gameId + ") in Riot database");
		
		try {
			elasticR.setGameTimeline(riotResponse, gameId);
			
		} catch (IOException e) {
			LOGGER.error("An error occured while persisting timeline of game with id: " + gameId,  e);
		}
		
		return riotResponse;
	}
	
}
