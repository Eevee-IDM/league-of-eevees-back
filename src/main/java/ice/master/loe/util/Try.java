package ice.master.loe.util;

/**
 * Represents the result of a computation.<br>
 * <br>
 * If {@link #isSuccess() successful}, an instance of this class
 * contains the result of the computation.<br>
 * <br>
 * If {@link #isFailure() failed}, an instance of this class has no
 * content but provides an error message.
 * 
 * @author Emmanuel CHEBBI
 *
 * @param <T> the type of the result
 */
public final class Try<T> {
	
	/** The result of the computation. null if the computation failed */
	private final T element;
	
	/** Indicates whether the instance is a success or a failure */
	private final boolean isSuccess;
	
	/** A message detailing the error. null if the computation succeeded */
	private final String error;
	
	/**
	 * Creates a new instance.
	 * 
	 * @param element
	 * 			The result of the computation.
	 * 			{@code null} if {@code isSuccess == false}.
	 * @param error
	 * 			The error message.
	 * 			{@code null} if {@code isSuccess == false}.
	 * @param isSuccess
	 * 			Indicates whether the instance represents a success of a failure.
	 */
	protected Try(T element, String error, boolean isSuccess) {
		this.error = error;
		this.element = element;
		this.isSuccess = isSuccess;
	}
	
	/**
	 * Returns the result of the computation.
	 * @return the result of {@code null} if {@code isSuccess() == false}
	 */
	public T get() {
		return element;
	}
	
	/**
	 * Returns the error message of the computation.
	 * @return the result of {@code null} if {@code isSuccess() == true}
	 */
	public String error() {
		return error;
	}
	
	/**
	 * Returns whether the instance corresponds to a success.
	 * @return whether the instance corresponds to a success
	 */
	public boolean isSuccess() {
		return isSuccess;
	}
	
	/**
	 * Returns whether the instance corresponds to a failure.
	 * @return whether the instance corresponds to a failure
	 */
	public boolean isFailure() {
		return ! isSuccess();
	}

	/**
	 * Creates a new {@code Try} instance corresponding to a successful computation.
	 * 
	 * @param element
	 * 			The result of the computation.
	 * 
	 * @return a new instance corresponding to a successful computation
	 */
	public static <T> Try<T> success(T element) {
		return new Try<>(element, "", true);
	}

	/**
	 * Creates a new {@code Try} instance corresponding to a failed computation.
	 * 
	 * @param error
	 * 			A message detailing the error
	 * 
	 * @return a new instance corresponding to a failed computation
	 */
	public static <T> Try<T> failure(String error) {
		return new Try<>(null, error, false);
	}
}