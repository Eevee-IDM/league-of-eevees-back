/**
 * Defines utility classes used across the project.
 * 
 * @author Emmanuel CHEBBI
 */
package ice.master.loe.util;