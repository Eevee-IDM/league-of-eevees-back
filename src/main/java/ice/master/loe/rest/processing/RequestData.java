package ice.master.loe.rest.processing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import ice.master.loe.model.loe.Request;
import ice.master.loe.model.loe.Summoner;
import ice.master.loe.model.loe.Timelapse;
import ice.master.loe.repository.GeneralRepository;
import ice.master.loe.rest.exceptions.ElementNotFoundException;
import ice.master.loe.rest.exceptions.GameNotFoundException;
import ice.master.loe.rest.exceptions.SummonerNotFoundException;

/**
 * Represents the data associated with a {@link Request}.
 * 
 * @author Léo CALVIS
 */
public class RequestData {
	
	/** Used for exceptions' error message */
	private static final String THE_GAME_DOES_NOT_EXIST_OR_IS_NOT_RELATED_TO_THE_REQUEST = ". The game does not exist or is not related to the request";
	
	/** Prevents SonarQube from complaining about duplicated Strings */
	private static final String GAME_ID = "gameId";

	/** Used to retrieve information from Riot */
	private static final GeneralRepository REPO = GeneralRepository.REPO;
	
	private final Map<String, String> summonersId = new HashMap<>();
	private final Map<String, String> summonersName = new HashMap<>();
	private final Map<String, JsonArray> summonersHistoric = new HashMap<>();
	private final Map<String, JsonObject> gamesStats = new HashMap<>(); 
	private final Map<String, JsonObject> gamesTimelines = new HashMap<>(); 
	
	/**
	 * Creates a new instance.
	 * 
	 * @param summoners
	 * 			The summoners concerned by the request.
	 * 			Must neither be {@code null} nor contain {@code null} elements.
	 * @param timelapse
	 * 			The period during which occurred the games to retrieve.
	 *  
	 * @throws IOException if an error occurs while querying request's data
	 */
	public RequestData(List<Summoner> summoners, Timelapse timelapse) throws IOException {
		this(summoners, timelapse, Collections.emptyMap());
	}
	
	/**
	 * Creates a new instance.
	 * 
	 * @param summoners
	 * 			The summoners concerned by the request.
	 * 			Must neither be {@code null} nor contain {@code null} elements.
	 * @param timelapse
	 * 			The period during which occurred the games to retrieve.
	 * @param filters
	 * 			Constraints on the which games should be considered.
	 * 			Must not be {@code null}.			
	 * 
	 * @throws IOException if an error occurs while querying request's data
	 */
	public RequestData(List<Summoner> summoners, Timelapse timelapse, Map<Summoner, RequestFilter> filters) throws IOException {
		for(Summoner sum : summoners) {
			String accountId = REPO.getAccountId(sum.getName())
								   .orElseThrow(() -> new SummonerNotFoundException("The summoner " + sum.getName() + " does not belong to any Riot account."));
			
			summonersId.put(sum.getName(), accountId);
			summonersName.put(summonersId.get(sum.getName()), sum.getName());
		}
		
		for(String sumId : summonersName.keySet()) {
			summonersHistoric.put(sumId, REPO.getHistoric(sumId, timelapse != null ? timelapse.getFrom().getTime() : null, endTime(timelapse), 0, 10000).getAsJsonArray("matches"));
		}
		
		for(Entry<Summoner, RequestFilter> entry : filters.entrySet()) {
			RequestFilter filter = entry.getValue();
			String summonerId = getSummonerId(entry.getKey().getName());
			
			if(filter.hasFilterOnChampions())
				filterHistory(summonerId, entry.getValue().getChampions(), "champion");
			if(filter.hasFilterOnLanes())
				filterHistory(summonerId, entry.getValue().getLanes(), "lane");
			if(filter.hasFilterOnResults())
				filterOnGameStat(summonerId, entry.getValue().getResults(), "win", true);
			if(filter.hasFilterOnTeams())
				filterOnGameStat(summonerId, entry.getValue().getTeams(), "teamId", false);
		}
	}

	/**
	 * Returns the timestamp of the last day of the timelapse, at 00 PM.
	 * Hence, all the games played during the last day of the timelapse are included as well.
	 * 
	 * @param timelapse
	 * 			The time range which end is desired.
	 * 
	 * @return the timestamp of the end of the timelapse, or {@code null} if no end of timelapse has been specified
	 */
	private Long endTime(Timelapse timelapse) {
		if (timelapse == null)
			return null;
		
		if (timelapse.getTo() == null)
			return System.currentTimeMillis();
		
		return timelapse.getTo().getTime() + 86_000_000;
	}
	
	/**
	 * Filters a summoner's history to keep only the games that satisfy a given property.
	 * 
	 * @param summonerId
	 * 			The id of the summoner to consider. 
	 * @param allowedValues
	 * 			The values allowed for the given property.
	 * 			All games having a different value for the property are removed from the history.
	 * @param thingToFilter
	 * 			The name of the property to check.
	 */
	private void filterHistory(String summonerId, Collection<String> allowedValues, String thingToFilter) {
		JsonArray history = summonersHistoric.get(summonerId);
		Iterator<JsonElement> itHistory = history.iterator();
		
		while (itHistory.hasNext()) {
			JsonElement game = itHistory.next();
		
			if (! allowedValues.contains(game.getAsJsonObject().get(thingToFilter).getAsString())) {
				itHistory.remove();
			}
		}
	}
	
	/**
	 * Filters a summoner's history to keep only the games having a certain statistic.
	 * 
	 * @param summonerId
	 * 			The id of the summoner to consider.
	 * @param allowedValues
	 * 			The values allowed for the given statistic.
	 * 			All the games having a different value are removed from the history.
	 * @param statisticToFilter
	 * 			The name of the statistic to consider.
	 * @param isInStatsDict
	 * 			Whether the statistic can be found in the dictionary returned by Riot.
	 */
	private void filterOnGameStat(String summonerId, Collection<String> allowedValues, String statisticToFilter, boolean isInStatsDict) {
		JsonArray history = summonersHistoric.get(summonerId);
		Iterator<JsonElement> itHistory = history.iterator();
		
		while (itHistory.hasNext()) {
			JsonElement element = itHistory.next();
		
			JsonPrimitive value = getStatisticValue(summonerId, element.getAsJsonObject().get(GAME_ID).getAsString(), statisticToFilter, isInStatsDict);
			
			if(! allowedValues.contains(value.getAsString()))
				itHistory.remove();
		}
	}
	
	/**
	 * Returns the value of a given statistic for a specific game.
	 * 
	 * @param summonerId
	 * 			The id of the summoner to consider.
	 * @param gameId
	 * 			The id of the game to look up.
	 * @param stat
	 * 			The name of the statistic to compute.
	 * @param isInStatsDict
	 * 			Whether the statistic can be found in the dictionary returned by Riot.
	 * 
	 * @return the value of {@code stat} in the game {@code gameId}
	 */
	public JsonPrimitive getStatisticValue(String summonerId, String gameId, String stat, boolean isInStatsDict) {
		JsonObject game = getGameStats(gameId);
		String participantId = findParticipantId(summonerId, game);
		
		for (JsonElement participant : game.getAsJsonArray("participants")) {
			JsonObject stats = participant.getAsJsonObject();
			
			if (isInStatsDict)
				stats = stats.getAsJsonObject("stats");
			
			if (stats.get("participantId").getAsString().equals(participantId)) {
				if(stats.has(stat))
					return stats.get(stat).getAsJsonPrimitive();
				else
					return new JsonPrimitive(0);
			}
		}
		
		// Throw an unchecked exception because it should never happen
		throw new ElementNotFoundException("Unable to find statistic " + stat + " of summoner with id: " + summonerId + " in game with id: " + gameId);
	}
	
	/**
	 * Returns the id of a player in a specific game.
	 * 
	 * @param accountId
	 * 			The id of the player's account
	 * @param game
	 * 			The game to check.
	 * 
	 * @return the id of the player in the given game
	 */
	private String findParticipantId(String accountId, JsonObject game) {
		for (JsonElement participant : game.getAsJsonArray("participantIdentities")) {
			if (participant.getAsJsonObject().get("player").getAsJsonObject().get("accountId").getAsString().equals(accountId)) {
				return participant.getAsJsonObject().get("participantId").getAsString();
			}
		}
		
		// Throw an unchecked exception because it should never happen
		throw new ElementNotFoundException("Unable to find participant ID of player with accountId " + accountId +" in game: " + game);
	}
	
	/**
	 * Returns the number of summoners associated with this request.
	 * @return the number of summoners associated with this request
	 */
	public int numberOfSummoners() {
		return summonersId.size();
	}
	
	/**
	 * Returns the number of games played by a summoner.
	 * 
	 * @param summonerId
	 * 			The id of the summoner to look up.
	 * 
	 * @return the number of games played by the summoner
	 * 
	 * @throws SummonerNotFoundException if {@code summonerId} does not match any summoner of the request
	 */
	public int numberOfGames(String summonerId) {
		JsonArray historic = summonersHistoric.get(summonerId);
		
		if (historic == null)
			throw new SummonerNotFoundException("Cannot find number of games of " + summonerId + THE_GAME_DOES_NOT_EXIST_OR_IS_NOT_RELATED_TO_THE_REQUEST);
		
		return historic.size();
	}
	
	/**
	 * Returns the id of a summoner from its name.
	 * 
	 * @param summonerName
	 * 			The name of the summoner.
	 * 
	 * @return the id of the summoner called {@code summonerName}
	 * 
	 * @throws SummonerNotFoundException if {@code summonerName} does not match any summoner of the request
	 */
	public String getSummonerId(String summonerName) {
		String id = summonersId.get(summonerName);
		
		if (id == null)
			throw new SummonerNotFoundException("Cannot find id of " + summonerName + THE_GAME_DOES_NOT_EXIST_OR_IS_NOT_RELATED_TO_THE_REQUEST);
	
		return id;
	}
	
	/**
	 * Returns the historic of a summoner.
	 * 
	 * @param summonerId
	 * 			The id of the summoner to look up.
	 * 
	 * @return the historic of the summoner identified by {@code summonerId}
	 * 
	 * @throws SummonerNotFoundException if {@code summonerId} does not match any summoner of the request
	 */
	public JsonArray getHistoric(String summonerId) {
		JsonArray historic = summonersHistoric.get(summonerId);
		
		if (historic == null)
			throw new SummonerNotFoundException("Cannot find historic of " + summonerId + THE_GAME_DOES_NOT_EXIST_OR_IS_NOT_RELATED_TO_THE_REQUEST);
	
		return historic;
	}
	
	/**
	 * Returns the statistics of a specific game.
	 * 
	 * @param gameId
	 * 			The id of the game to look up.
	 * 
	 * @return the statistics of the game identified by {@code gameId}
	 * 
	 * @throws GameNotFoundException if {@code gameId} does not match any game relative to this request
	 */
	public JsonObject getGameStats(String gameId) {
		if (gamesStats.isEmpty())
			collectGamesStats();
		
		return gamesStats.computeIfAbsent(gameId, REPO::getGameStats);
	}

	/**
	 * Collects the statistics about all the games of all the summoners involved in the request.<br>
	 * <br>
	 * Collecting all the statistics at reduces the number of queries that are sent to Riot or to 
	 * the database and hence improve the performances.
	 */
	private void collectGamesStats() {
		
		for (String summonerId : summonersId.values()) {
			List<String> gameIds = new ArrayList<>();

			for (JsonElement e : getHistoric(summonerId)) {
				gameIds.add(e.getAsJsonObject().get(GAME_ID).getAsString());
			}

			List<JsonObject> games = REPO.getGamesStats(gameIds);

			for (JsonObject game : games) {
				gamesStats.put(game.get(GAME_ID).getAsString(), game);
			}
		}
		
	}
	
	/**
	 * Returns the timeline of a specific game.
	 * 
	 * @param gameId
	 * 			The id of the game to look up.
	 * 
	 * @return the timeline of the game identified by {@code gameId}
	 * 
	 * @throws IOException if an error occurs while querying Elasticsearch database
	 */
	public JsonObject getGameTimeline(String gameId) throws IOException {
		if (gamesTimelines.containsKey(gameId))
			return gamesTimelines.get(gameId);
		
		JsonObject timeline = REPO.getGameTimeline(gameId);
		gamesTimelines.put(gameId, timeline);
		return timeline;
	}

}
	