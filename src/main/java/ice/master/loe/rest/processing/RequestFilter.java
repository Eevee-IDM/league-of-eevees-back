package ice.master.loe.rest.processing;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import ice.master.loe.model.loe.ChampionValue;
import ice.master.loe.model.loe.LaneValue;
import ice.master.loe.model.loe.Result;
import ice.master.loe.model.loe.ResultValue;
import ice.master.loe.model.loe.TeamValue;
import ice.master.loe.model.loe.ValueType;

/**
 * Keeps all the values allowed by the request's creator for given {@link ValueType ValueTypes}.
 * 
 * @author Léo Calvis
 */
class RequestFilter {
	
	private final Set<String> champions = new HashSet<>();
	private final Set<String> lanes = new HashSet<>();
	private final Set<String> results = new HashSet<>();
	private final Set<String> teams = new HashSet<>();
	
	/**
	 * Allows a new Champion to be played.<br>
	 * <br>
	 * The games where this champion is not played 
	 * won't be taken into account.
	 * 
	 * @param champion
	 * 			The champion to allow.
	 */
	public void addChampion(ChampionValue champion) {
		champions.add("" + champion.getChampion().getValue());
	}
	
	/**
	 * Returns all the champions allowed for the request.<br>
	 * <br>
	 * <b>Caution:</b> an empty list means that <b>all</b> the champions
	 * are allowed.
	 * 
	 * @return all the champions allowed for the request
	 */
	public Collection<String> getChampions() {
		return champions;
	}
	
	public boolean hasFilterOnChampions() {
		return !champions.isEmpty();
	}
	
	/**
	 * Allows a new lane to be played.<br>
	 * <br>
	 * The games where this lane is not played 
	 * won't be taken into account.
	 * 
	 * @param lane
	 * 			The lane to allow.
	 */
	public void addLane(LaneValue lane) {
		lanes.add(lane.getLane().getRiotId());
	}
	
	/**
	 * Returns all the lanes allowed for the request.<br>
	 * <br>
	 * <b>Caution:</b> an empty list means that <b>all</b> the lanes
	 * are allowed.
	 * 
	 * @return all the lanes allowed for the request
	 */
	public Collection<String> getLanes() {
		return lanes;
	}
	
	public boolean hasFilterOnLanes() {
		return !lanes.isEmpty();
	}
	
	/**
	 * Allows a new game result to happen.<br>
	 * <br>
	 * The games that does not end with the specified result
	 * won't be taken into account.
	 * 
	 * @param result
	 * 			The result to allow.
	 */
	public void addResult(ResultValue result) {
		results.add((String.valueOf(result.getResult().equals(Result.WIN))));
	}
	
	/**
	 * Returns all the game results allowed for the request.<br>
	 * <br>
	 * <b>Caution:</b> an empty list means that every game result is allowed.
	 * 
	 * @return all the game results allowed for the request
	 */
	public Collection<String> getResults() {
		return results;
	}
	
	public boolean hasFilterOnResults() {
		return !results.isEmpty();
	}
	
	/**
	 * Allows a new Team to be played in.<br>
	 * <br>
	 * The games where the player is not in the specified team 
	 * won't be taken into account.
	 * 
	 * @param team
	 * 			The team to allow.
	 */
	public void addTeam(TeamValue team) {
		teams.add(team.getTeam().getRiotId());
	}
	
	/**
	 * Returns all the teams allowed for the request.<br>
	 * <br>
	 * <b>Caution:</b> an empty list means that <b>all</b> the teams
	 * are allowed.
	 * 
	 * @return all the teams allowed for the request
	 */
	public Collection<String> getTeams() {
		return teams;
	}
	
	public boolean hasFilterOnTeams() {
		return !teams.isEmpty();
	}
	
}
