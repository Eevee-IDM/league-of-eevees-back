package ice.master.loe.rest.processing;

import static java.util.Objects.requireNonNull;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import ice.master.loe.model.loe.Champion;
import ice.master.loe.model.loe.ChampionValue;
import ice.master.loe.model.loe.Computation;
import ice.master.loe.model.loe.ComputedYValue;
import ice.master.loe.model.loe.FilterByValue;
import ice.master.loe.model.loe.Lane;
import ice.master.loe.model.loe.LaneValue;
import ice.master.loe.model.loe.Request;
import ice.master.loe.model.loe.Result;
import ice.master.loe.model.loe.ResultValue;
import ice.master.loe.model.loe.Stat;
import ice.master.loe.model.loe.StatValue;
import ice.master.loe.model.loe.Summoner;
import ice.master.loe.model.loe.Team;
import ice.master.loe.model.loe.TeamValue;
import ice.master.loe.model.loe.TimeScale;
import ice.master.loe.model.loe.Timelapse;
import ice.master.loe.model.loe.Value;
import ice.master.loe.model.loe.ValueType;

/**
 * Interprets a {@link Request}.<br>
 * <br>
 * Processes a request, queries the related information, then returns it in the form of a JSON.
 * 
 * @author Léo Calvis
 */
public class RequestProcess {
	
	private static final Logger LOGGER = LogManager.getLogger(RequestProcess.class.getName());
	
	private static final JsonParser JPARSER = new JsonParser();
	
	/** Prevents SonarQube from complaining for duplicate Strings */
	private static final String GAMES = " games";
	
	/** The request to interpret */
	private final Request request;
	
	private RequestData data;
	private Map<Summoner, RequestFilter> filters;
	private List<String> indices;
	
	private ValueType unitSplitter;
	private TimeScale timeSpliter;
	
	/** Cache the result of the request so that it is not re-computed */
	private String result;
	
	/**
	 * Creates a new object able to interpret {@code request}.
	 * 
	 * @param request
	 * 			The request to interpret.
	 * 			Must not be {@code null}.
	 */
	public RequestProcess(Request request) {
		this.request = requireNonNull(request, "Cannot process a null request");
	}
	
	/**
	 * Returns the request interpreted by this object.
	 * @return the request interpreted by this object
	 */
	public Request getRequest() {
		return this.request;
	}
	
	/**
	 * Returns the result of the request.<br>
	 * <br>
	 * The returned value is cached so that the interpretation is done only once.
	 * 
	 * @return a JSON-formatted result
	 * 
	 * @throws IOException if an error occurs while querying the data
	 */
	public String getResult() throws IOException {
		if (result == null)
			return exec();

		return result;
	}
	
	/** @return an object storing request-related information in order to ease its interpretation */
	private RequestData prepareData() throws IOException {
		filters = new HashMap<>();
		Timelapse timelapse =  request.getTimelapse();
		
		LOGGER.info("Number of filters: " + request.getFilters().size());
		
		for (Summoner summoner : request.getSummoners()) {
			RequestFilter reqFilter = new RequestFilter();
			
			for (FilterByValue filter : request.getFilters().stream().map(FilterByValue.class::cast).collect(Collectors.toList())) {
				Value filterValue = filter.getValue();
				if(filterValue instanceof ChampionValue)
					reqFilter.addChampion((ChampionValue)filterValue);
				else if(filterValue instanceof LaneValue)
					reqFilter.addLane((LaneValue)filterValue);
				else if(filterValue instanceof ResultValue)
					reqFilter.addResult((ResultValue)filterValue);
				else if(filterValue instanceof TeamValue)
					reqFilter.addTeam((TeamValue)filterValue);
				LOGGER.info("Filter : " + filterValue.getType().getLiteral());
			}
			
			filters.put(summoner, reqFilter);
		}
		
		return new RequestData(request.getSummoners(), timelapse, filters);
	}
	
	/**
	 * Interprets the request.
	 * 
	 * @return the result of the request in JSON format
	 * 
	 * @throws IOException if an error occurs while querying the data
	 */
	private String exec() throws IOException {
		
		data = prepareData();
		
		indices = new ArrayList<>();
		
		List<ComputedYValue> dataToRetrieve = request.getYvalues().stream()
				.map(ComputedYValue.class::cast)
				.collect(Collectors.toList());
		
		unitSplitter = request.getXUnit();
		
		timeSpliter = request.getTimeScale();
		
		String baseStringJson = "{\"id\":\"" + request.hashCode() + "Evoli" + System.nanoTime() + "\", \"code\": 200,\"data\":[],\"indices\":[],\"date\":{\"from\":\"\",\"to\":\"\"},\"display\":{\"name\":\"" + request.getDiagram().getName() + "\",\"type\":\"" + request.getDiagram().getType().getId() + "\"}}";
		JsonObject jsonResult = JPARSER.parse(baseStringJson).getAsJsonObject();
		JsonArray dataResult = jsonResult.getAsJsonArray("data");
		
		for(Summoner summoner : request.getSummoners()) {
			findNeededInformation(dataToRetrieve, dataResult, summoner);
		}
		
		JsonArray indicesArray = jsonResult.get("indices").getAsJsonArray();
		
		if(timeSpliter != TimeScale.ALL_THE_TIME)
			Collections.sort(indices);
		
		for(String i : indices)
			indicesArray.add(i);
		
		result = jsonResult.toString();
		LOGGER.info(jsonResult.toString());
		
		return result;
	}

	/**
	 * Finds some informations relative to a summoner.
	 * 
	 * @param dataToRetrieve
	 * 			The list of data to retrieve.
	 * @param dataResult
	 * 			The JSON to enhance with found data.
	 * @param summoner
	 * 			The summoner which information are looked for.
	 */
	private void findNeededInformation(List<ComputedYValue> dataToRetrieve, JsonArray dataResult, Summoner summoner) {
		String summonerName = summoner.getName();
		String accountId = this.data.getSummonerId(summonerName);
		
		JsonObject statSummonerJson = JPARSER.parse("{\"summoner\":\"" + summonerName + "\",\"courbeName\":\"noSense\"}").getAsJsonObject();
		
		JsonArray courbeDataJson = new JsonArray();

		for (ComputedYValue dataType : dataToRetrieve)
			
			if(timeSpliter != TimeScale.ALL_THE_TIME) {
				
				statSummonerJson = JPARSER.parse("{\"summoner\":\"" + summonerName + "\",\"courbeName\":\"noSense\"}").getAsJsonObject();
				courbeDataJson = new JsonArray();
				
				findInfoSpliByTime(accountId, statSummonerJson, courbeDataJson, dataType);
				
				statSummonerJson.add("courbeSummoner", courbeDataJson);
				dataResult.add(statSummonerJson);
				
			} else {
				if(unitSplitter != ValueType.NONE)
					findNeededInformationWithUnitSplit(dataType, accountId, data.getHistoric(accountId), courbeDataJson, summoner);
				else
					findOneNeededInformation(dataType, accountId, data.getHistoric(accountId), courbeDataJson, "");
			}
		
		if(timeSpliter == TimeScale.ALL_THE_TIME) {
			statSummonerJson.add("courbeSummoner", courbeDataJson);
			dataResult.add(statSummonerJson);
		}
		
		
		
	}

	private void findInfoSpliByTime(String accountId, JsonObject statSummonerJson, JsonArray courbeDataJson,
			ComputedYValue dataType) {
		SortedMap<String, JsonArray> splitedHist = new TreeMap<>();
				
		getHistoricByTime(data.getHistoric(accountId), splitedHist, timeSpliter);
		
		for(Map.Entry<String, JsonArray> e : splitedHist.entrySet()) {
			
			int posBefore = courbeDataJson.size();
			
			findOneNeededInformation(dataType, accountId, e.getValue(), courbeDataJson, "");
			
			int pos = courbeDataJson.size();
			if(pos - posBefore != 0)
				courbeDataJson.get(courbeDataJson.size()-1).getAsJsonObject().addProperty("label", e.getKey());
			
			statSummonerJson.addProperty("courbeName", indices.remove(indices.size()-1));
			
			if (! indices.contains(e.getKey()))
				indices.add(e.getKey());
			
		}
		
		Collections.sort(indices);
	}
	
	private void findNeededInformationWithUnitSplit(ComputedYValue dataType, String accountId, JsonArray historic, JsonArray courbeDataJson, Summoner summoner) {
		Map<String, JsonArray> splitedHist = getHistoricBy(accountId, historic, unitSplitter);

		switch(unitSplitter) {
		case CHAMPION:
			splitOnChampion(dataType, accountId, courbeDataJson, summoner, splitedHist);
			break;
		
		case LANE:
			splitOnLane(dataType, accountId, courbeDataJson, summoner, splitedHist);
			break;
			
		case TEAM: 
			splitOnTeam(dataType, accountId, courbeDataJson, summoner, splitedHist);
			break;
			
		case GAME_RESULT:
			splitOnGameResult(dataType, accountId, courbeDataJson, summoner, splitedHist);
			break;
			
		default:
		}
			
	}

	private void splitOnGameResult(ComputedYValue dataType, String accountId, JsonArray courbeDataJson,
			Summoner summoner, Map<String, JsonArray> splitedHist) {
		for(Result r : Result.VALUES) {
			
			if(filters.get(summoner).hasFilterOnResults() && !filters.get(summoner).getResults().contains(Integer.toString(r.getValue() == Result.WIN_VALUE ? Result.WIN_VALUE : Result.LOSE_VALUE)))
				continue;
			
			findOneNeededInformationValidate(dataType, accountId, splitedHist.get(Integer.toString(r.getValue() == Result.WIN_VALUE ? Result.WIN_VALUE : Result.LOSE_VALUE)), courbeDataJson, r.getName());
		}
	}

	private void splitOnTeam(ComputedYValue dataType, String accountId, JsonArray courbeDataJson, Summoner summoner,
			Map<String, JsonArray> splitedHist) {
		for(Team t : Team.VALUES) {
			
			if(filters.get(summoner).hasFilterOnTeams() && !filters.get(summoner).getTeams().contains(t.getRiotId()))
				continue;
			
			findOneNeededInformationValidate(dataType, accountId, splitedHist.get(t.getRiotId()), courbeDataJson, t.getRiotName());
		}
	}

	private void splitOnLane(ComputedYValue dataType, String accountId, JsonArray courbeDataJson, Summoner summoner,
			Map<String, JsonArray> splitedHist) {
		for(Lane l : Lane.VALUES) {
			
			if(filters.get(summoner).hasFilterOnLanes() && !filters.get(summoner).getLanes().contains(l.getRiotId()))
				continue;
			
			findOneNeededInformationValidate(dataType, accountId, splitedHist.get(l.getRiotId()), courbeDataJson, l.getRiotName());
		}
	}

	private void splitOnChampion(ComputedYValue dataType, String accountId, JsonArray courbeDataJson, Summoner summoner,
			Map<String, JsonArray> splitedHist) {
		List<Champion> champions = new ArrayList<>(Champion.VALUES);
		Collections.sort(champions, (ch1, ch2) -> ch1.getRiotName().compareTo(ch2.getRiotName()));
		for(Champion c : champions) {
			
			if(filters.get(summoner).hasFilterOnChampions() && !filters.get(summoner).getChampions().contains(c.getRiotId()))
				continue;
			
			findOneNeededInformationValidate(dataType, accountId, splitedHist.get(c.getRiotId()), courbeDataJson, c.getRiotName());
		}
	}
	
	private void findOneNeededInformationValidate(ComputedYValue dataType, String accountId, JsonArray historyWithOneThing, JsonArray courbeDataJson, String name) {
		if(historyWithOneThing == null) {
			if(dataType.getComputation().getValue() == Computation.SUM_VALUE)
				findOneNeededInformation(dataType, accountId, new JsonArray(), courbeDataJson, name + " ");
		} else
			findOneNeededInformation(dataType, accountId, historyWithOneThing, courbeDataJson, name + " ");
	}
	
	private void findOneNeededInformation(ComputedYValue dataType, String accountId, JsonArray historic, JsonArray courbeDataJson, String label) {
		
		if (! (dataType.getValue() instanceof Value))
			return;
		
		Value dataValue = ((Value) dataType.getValue());

		long value = 0;

		if (dataValue instanceof ResultValue) {

			ResultValue resultValue = (ResultValue) dataValue;

			switch (resultValue.getResult()) {
			case WIN:
				value = getStat(accountId, historic, null, "win", null, true);
				label += "victories";
				break;
			case LOSE:
				value = historic.size() - getStat(accountId, historic, null, "win", null, true);
				label += "defeats";
				break;
			default:
				break;
			}

		} else if (dataValue instanceof LaneValue) {

			Lane lane = ((LaneValue) dataValue).getLane();

			label += lane.getRiotName() + GAMES;
			value = getNumberGamesOnLane(historic, lane.getRiotId());

		} else if (dataValue instanceof TeamValue) {

			Team team = ((TeamValue) dataValue).getTeam();

			label += team.getRiotName() + GAMES;
			value = getStat(accountId, historic, null, "teamId", team.getRiotId(), false);

		} else if (dataValue instanceof ChampionValue) {

			Champion champion = ((ChampionValue) dataValue).getChampion();
			
			label = champion.getRiotName() + GAMES;
			value = getNumberGamesOnChampion(historic, "" + champion.getValue());

		} else if (dataValue instanceof StatValue) {

			Stat stat = ((StatValue) dataValue).getStat();

			label += stat.getRiotName();
			value = getStat(accountId, historic, null, stat.getRiotId(), null, true);

		}

		switch (dataType.getComputation()) {
		case SUM:
			courbeDataJson.add(JPARSER.parse("{\"label\": \"" + label + "\",\"value\":" + value + "}"));
			break;
		case AVERAGE:
			if(historic.size() < 1)
				return;
			double average = ((double) value) / historic.size();
			courbeDataJson.add(JPARSER.parse("{\"label\":\"" + label + "\",\"value\":" + formatDouble(average) + "}"));
			break;
		case PERCENTAGE:
			if(historic.size() < 1)
				return;
			double percentage = ((double) value) / historic.size() * 100;
			courbeDataJson.add(JPARSER.parse("{\"label\":\"" + label + "\",\"value\":\"" + formatDouble(percentage) + "%\"}"));
			break;
		default:
			break;
		}
		
		if (! indices.contains(label))
			indices.add(label);
		
	}
	
	private static String formatDouble(Double val) {
		DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
		decimalFormatSymbols.setDecimalSeparator('.');
		decimalFormatSymbols.setGroupingSeparator(' ');
		DecimalFormat decimalFormat = new DecimalFormat("###0.00", decimalFormatSymbols);
		return decimalFormat.format(val);
	}
	
	/**
	 * Retrieves the value of a given statistic for a specific player.
	 * 
	 * @param accountId
	 * 			Identifies the account of the player.
	 * @param timelapse
	 * 			The range of time in which pick the games used to compute the statistic.
	 * @param stat
	 * 			The name of the statistic to compute.
	 * @param value
	 * 			The value of the statistic to look for. Can be {@code null} to match
	 * 			any value.
	 * @param isInStatsDict
	 * 			Returns whether the statistic is within the dictionary returned by Riot.
	 * 
	 * @return the value of the statistics.
	 */
	private long getStat(String accountId, JsonArray historic, Timelapse timelapse, String stat, String value, boolean isInStatsDict) {
		
		long sum = 0;
		long begin = Long.MAX_VALUE;
		long end = Long.MIN_VALUE;

		if (timelapse != null) {
		    begin = timelapse.getFrom().getTime();
		    end = timelapse.getTo().getTime();
		}

		LOGGER.info("About to iterate over an historic of size: " + historic.size());
		
		for (JsonElement game : historic) {
		    long timestamp = Long.parseLong(game.getAsJsonObject().get("timestamp").getAsString());

		    if (begin < timestamp || timestamp < end)
		        continue;

		    String gameId = game.getAsJsonObject().get("gameId").getAsString();

		    sum += sumStat(gameId, accountId, stat, value, isInStatsDict);
		}
		return sum;
	}
	
	/**
	 * Sums a statistic for a given game.
	 * 
	 * @param sum
	 * 			The current value of the statistic.
	 * @param gameId
	 * 			The id of the game.
	 * @param accountId
	 * 			The id of the player's account.
	 * @param stat
	 * 			The name of the statistic to sum.
	 * @param value
	 * 			The value to look for, if any. Can be {@code null} to match any value.
	 * @param isInStatsDict
	 * 			Whether the statistic is in the dictionary returned by Riot.
	 */
	private long sumStat(String gameId, String accountId, String stat, String value, boolean isInStatsDict) {
		JsonPrimitive statistic = data.getStatisticValue(accountId, gameId, stat, isInStatsDict);

		if (statistic.isBoolean())
			return statistic.getAsBoolean() ? 1 : 0;

		// In case we're looking for a precise value
		if (value != null)
			return statistic.getAsString().equals(value) ? 1 : 0;

		return statistic.getAsLong();
	}
	
	/**
	 * Returns the historic of a summoner splited by the defined Unit.
	 * 
	 * @param summonerId
	 * 			The id of the summoner to look up.
	 * 
	 * @param unit
	 * 			The type with which the historic is splitted
	 * 
	 * @return the historic of the summoner identified by {@code summonerId}
	 */
	private Map<String, JsonArray> getHistoricBy(String summonerId, JsonArray historic, ValueType unit) {
		
		Map<String, JsonArray> resultHistorys = new HashMap<>();
		
		switch(unit) {
		case CHAMPION:
			getHistoricByOnHistory(historic, resultHistorys, "champion");
			break;
			
		case LANE:
			getHistoricByOnHistory(historic, resultHistorys, "lane");
			break;
			
		case TEAM:
			getHistoricByOnGame(historic, summonerId, resultHistorys, "teamId", false);
			break;
			
		case GAME_RESULT:
			getHistoricByOnGame(historic, summonerId, resultHistorys, "win", true);
			break;
			
		default:
			
		}
			
	
		return resultHistorys;
	}
	
	
	private static void getHistoricByTime(JsonArray historic, Map<String, JsonArray> resultHistorys, TimeScale scale) {
		
		for(JsonElement e : historic) {
			
			String periode = "";
			
			long gameDate = e.getAsJsonObject().get("timestamp").getAsLong();
			
			switch(scale.getValue()) {
			case TimeScale.DAY_VALUE:
				periode = new LocalDate(gameDate, DateTimeZone.UTC).toString("yyyy-MM-dd");
				break;
				
			case TimeScale.MONTH_VALUE:
				periode = new LocalDate(gameDate, DateTimeZone.UTC).toString("yyyy-MM");
				break;
				
			case TimeScale.YEAR_VALUE:
				periode = new LocalDate(gameDate, DateTimeZone.UTC).toString("yyyy");
				break;
				
			default:
				
			}
			
			if(!resultHistorys.containsKey(periode))
				resultHistorys.put(periode, new JsonArray());
			
			resultHistorys.get(periode).add(e);
			
		}
		
		
	}
	

	private static void getHistoricByOnHistory(JsonArray historic, Map<String, JsonArray> resultHistorys, String type) {
		for (JsonElement game : historic) {
			String id = game.getAsJsonObject().get(type).getAsString();
			
			if(!resultHistorys.containsKey(id))
				resultHistorys.put(id, new JsonArray());
				
			resultHistorys.get(id).add(game);
		}
	}
	
	private void getHistoricByOnGame(JsonArray historic, String accountId, Map<String, JsonArray> resultHistorys, String type, boolean isInStatsDict) {
		for (JsonElement game : historic) {
			String gameId = game.getAsJsonObject().get("gameId").getAsString();
			
			String id = Long.toString(sumStat(gameId, accountId, type, null, isInStatsDict));
			
			if(!resultHistorys.containsKey(id))
				resultHistorys.put(id, new JsonArray());
				
			resultHistorys.get(id).add(game);
		}
	}
	
	/**
	 * Returns the number of games played in a given lane.
	 * 
	 * @param accountId
	 * 			The account of the player.
	 * @param lane
	 * 			The lane played.
	 * 
	 * @return the number of games played in {@code lane}.
	 */
	private static int getNumberGamesOnLane(JsonArray historic, String lane) {
		return getNumberGamesOnX(historic, "lane", lane);
	}
	
	/**
	 * Returns the number of games played with a given champion.
	 * 
	 * @param accountId
	 * 			The account of the player.
	 * @param championId
	 * 			The id of the champion played.
	 * 
	 * @return the number of games played in {@code lane}.
	 */
	private static int getNumberGamesOnChampion(JsonArray historic, String championId) {
		return getNumberGamesOnX(historic, "champion", championId);
	}
	
	/**
	 * Returns the number of games that match a given criteria.
	 * 
	 * @param accountId
	 * 			The account of the player.
	 * @param type
	 * 			The type of the value to look for.
	 * @param value
	 * 			The value to match.
	 * 
	 * @return the number of games that match a given criteria
	 */
	private static int getNumberGamesOnX(JsonArray historic, String type, String value) {
		int numberOfGames = 0;
		
		for (JsonElement game : historic) {
			if (game.getAsJsonObject().get(type).getAsString().equals(value))
				numberOfGames += 1;
		}
		
		return numberOfGames;
	}
}
