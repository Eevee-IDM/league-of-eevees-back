/**
 * Define endpoints aimed to deal with user's favorites
 * 
 * @author Emmanuel CHEBBI
 */
package ice.master.loe.rest.bookmarks;