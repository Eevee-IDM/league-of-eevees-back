package ice.master.loe.rest.bookmarks;

import static java.util.Arrays.asList;

import java.util.Optional;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import ice.master.loe.model.loe.LoeFactory;
import ice.master.loe.model.loe.Request;
import ice.master.loe.model.loe.Summoner;
import ice.master.loe.repository.GeneralRepository;
import ice.master.loe.repository.elastic.ElasticsearchBookmarks;
import ice.master.loe.rest.Errors;
import ice.master.loe.rest.gloe.GloeParser;
import ice.master.loe.rest.processing.RequestData;
import ice.master.loe.util.Try;
import lombok.extern.log4j.Log4j2;

/**
 * REST service providing endpoints aimed to manage user's favorites.
 * 
 * @author Emmanuel CHEBBI
 */
@Log4j2
@Path("/bookmarks")
public class BookmarksEndpoint {
	
	private final ElasticsearchBookmarks db = new ElasticsearchBookmarks();
	
    @GET
    @Path("/all")
    @Produces(MediaType.TEXT_PLAIN)
    public String getAllFavorites(@QueryParam("summoner") String summonerName) {
    	try {
	    	String accountId = new RequestData(asList(asSummoner(summonerName)), null).getSummonerId(summonerName);
	    	
	    	JsonArray bookmarks = db.getAllBookmarks(accountId);
	    	
	    	JsonObject response = new JsonObject();
	    	response.addProperty("code", 200);
	    	response.add("bookmarks", bookmarks);
	    	return response.toString();
	    	
    	} catch (Exception e) {
    		log.error("An error occured while retrieving favorites of summoner: " + summonerName, e);
    		return Errors.INTERNAL_PROCESSING_ERROR.asJSON();
    	}
    }
    
    /** @return a new {@code Summoner} called {@code name} */
    private Summoner asSummoner(String name) {
    	Summoner summoner = LoeFactory.eINSTANCE.createSummoner();
    	summoner.setName(name);
    	return summoner;
    }
    
    @GET
    @Path("/snapshot")
    @Produces(MediaType.TEXT_PLAIN)
    public String getSnapshot(@QueryParam("summoner") String summonerName) {
    	try {
    		String accountId = new RequestData(asList(asSummoner(summonerName)), null).getSummonerId(summonerName);
    		
    		JsonArray snapshots = new JsonArray();
    		JsonArray bookmarks = db.getAllBookmarks(accountId);
    		
    		for (JsonElement element : bookmarks) {
    			JsonObject snapshot = new JsonObject();
    			JsonObject bookmark = element.getAsJsonObject();
    			
    			Try<Request> request = new GloeParser().parse(bookmark.get("query").getAsString());
    			String diagramType = request.isSuccess() ? request.get().getDiagram().getType().getId() : "<unparseable query>";
    			
				snapshot.add("id", bookmark.get("id"));
				snapshot.addProperty("diagramType", diagramType);
				
    			snapshots.add(snapshot);
    		}
    		
    		JsonObject response = new JsonObject();
    		response.addProperty("code", 200);
    		response.add("bookmarks", snapshots);
    		return response.toString();
    		
    	} catch (Exception e) {
    		log.error("An internal error occurred while retrieving favorites of summoner: " + summonerName, e);
    		return Errors.INTERNAL_PROCESSING_ERROR.asJSON();
    	}
    }
    
    @GET
    @Path("/delete")
    @Produces(MediaType.TEXT_PLAIN)
    public String deleteFromFavorites(@QueryParam("id") String bookmarkId) {
    	try {
			db.deleteBookmark(bookmarkId);
			return Errors.SUCCESS.asJSON();
			
		} catch (Exception e) {
			log.error("An error occurred while deleted bookmark with id: " + bookmarkId, e);
			return Errors.INTERNAL_PROCESSING_ERROR.asJSON();
		}
    }
    
    @GET
    @Path("/create")
    @Produces(MediaType.TEXT_PLAIN)
    public String createFavorite(@QueryParam("bookmarkId") String bookmarkId, @QueryParam("summonerName") String summonerName, @QueryParam("title") String title, @QueryParam("query") String query) {
    	try {
    		Optional<String> accountId = GeneralRepository.REPO.getAccountId(summonerName);
    		
    		if (! accountId.isPresent())
    			return Errors.SUMMONER_DOES_NOT_EXIST.asJSON("The summoner " + summonerName + " does not exist");
    				
    		JsonObject bookmark = new JsonObject();
    		bookmark.addProperty("id", bookmarkId);
    		bookmark.addProperty("accountId", accountId.get());
    		bookmark.addProperty("title", title);
    		bookmark.addProperty("query", query);
    		
			db.createBookmark(bookmarkId, bookmark);
			return Errors.SUCCESS.asJSON();
			
		} catch (Exception e) {
			log.error("An error occurred while creating a new bookmark (bookmarkId: " + bookmarkId + ", summonerName: " + summonerName + ", title: " + title + ", query: " + query + ")", e);
			return Errors.INTERNAL_PROCESSING_ERROR.asJSON();
		}
    }
    
    @GET
    @Path("/get")
    @Produces(MediaType.TEXT_PLAIN)
    public String getFavorite(@QueryParam("bookmarkId") String bookmarkId) {
    	try {
    		Optional<JsonObject> bookmark = db.getBookmark(bookmarkId);
    		
    		if (! bookmark.isPresent())
    			return Errors.BOOKMARK_DOES_NOT_EXIST.asJSON("no bookmark can be found with id: " + bookmarkId);
    		
        	JsonObject response = new JsonObject();
        	response.addProperty("code", 200);
        	response.add("bookmark", bookmark.get());
        	
        	return response.toString();
    		
    	} catch(Exception e) {
    		log.error("An error occurred while retrieve bookmark with id: " + bookmarkId, e);
    		return Errors.INTERNAL_PROCESSING_ERROR.asJSON();
    	}
    }
}
