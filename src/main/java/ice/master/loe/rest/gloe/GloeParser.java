package ice.master.loe.rest.gloe;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.resource.XtextResource;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.eclipse.xtext.util.CancelIndicator;
import org.eclipse.xtext.validation.CheckMode;
import org.eclipse.xtext.validation.IResourceValidator;
import org.eclipse.xtext.validation.Issue;

import com.google.inject.Injector;

import ice.master.loe.model.loe.Request;
import ice.master.loe.rest.Errors;
import ice.master.loe.util.Try;

/**
 * Utility class aimed to isolate the parsing of a Gloe request.
 * 
 * @author Emmanuel CHEBBI
 */
public class GloeParser {

	/**
	 * Parses {@code query}.<br>
	 * <br>
	 * If the query is well-formatted, returns a successful {@code Either} containing
	 * a {@link Request} instance.<br>
	 * <br>
	 * Otherwise, returns a failed {@code Either} which error message is a JSON describing
	 * the error.
	 * 
	 * @param query
	 * 			The query to parse. Should follow the Gloe grammar.
	 * 
	 * @return a successful {@code Either} if the query can be parsed, 
	 * 		   a failed {@code Either} otherwise.
	 */
	public Try<Request> parse(String query) {
    	if (query == null || query.trim().isEmpty())
    		return Try.failure(Errors.EMPTY_REQUEST.asJSON());
    	
    	Injector injector = GloeInjector.get();
    	ResourceSet resources = injector.getInstance(XtextResourceSet.class);
		
		Resource resource = resources.createResource(URI.createURI("dummy:/temporary.gloe"));
		InputStream in = new ByteArrayInputStream(query.getBytes());
		
		try {
			resource.load(in, resources.getLoadOptions());
			
		} catch (IOException e) { // Should never happen
			Try.failure(Errors.INTERNAL_PARSING_ERROR.asJSON());
		}
		
		// Should always be true
		if (resource instanceof XtextResource) {
			XtextResource xtextResource = (XtextResource) resource;
			
			// Search for syntax errors
			if (xtextResource.getParseResult().getSyntaxErrors().iterator().hasNext())
				return Try.failure(createMalformedQueryErrorMessage(xtextResource.getParseResult().getSyntaxErrors()));
			
			// Search for validation errors
			IResourceValidator validator = xtextResource.getResourceServiceProvider().getResourceValidator();
			List<Issue> issues = validator.validate(resource, CheckMode.ALL, CancelIndicator.NullImpl);
			
			if (! issues.isEmpty())
				return Try.failure(createIncorrectQueryErrorMessage(issues));
		}
		
		if (resource.getContents().isEmpty())
			return Try.failure(Errors.EMPTY_REQUEST.asJSON());
		
		Request request = (Request) resource.getContents().get(0);
		return Try.success(request);
	}
	
	private String createMalformedQueryErrorMessage(Iterable<INode> errors) {
		StringBuilder message = new StringBuilder();
		
		for (INode error : errors) {
			message.append(error.getSyntaxErrorMessage().getMessage()).append("\n");
		}
		
		return Errors.asJSON(Errors.MALFORMED_REQUEST.code, message.substring(0, message.length() - 1));
	}
	
	private String createIncorrectQueryErrorMessage(Iterable<Issue> errors) {
		StringBuilder message = new StringBuilder();
		
		for (Issue error : errors) {
			message.append(error.getMessage()).append("\n");
		}
		
		return Errors.asJSON(Errors.INCORRECT_REQUEST.code, message.substring(0, message.length() - 1));
	}
}
