package ice.master.loe.rest.gloe;

import com.google.inject.Injector;

/**
 * Represents the injector associated with {@link GloeWebSetup}.<br>
 * <br>
 * This singleton prevents the EMF registration from being done multiple times,
 * which would end up with a Guice exception.
 * 
 * @author Emmanuel CHEBBI
 */
public enum GloeInjector {
	
	INSTANCE;
	
	private static Injector injector = null;
	
	/**
	 * Get the {@link GloeWebSetup} injector.<br>
	 * <br>
	 * If the injector does not exist yet, it is created
	 * through {@link GloeWebSetup#createInjectorAndDoEMFRegistration()}.
	 * 
	 * @return the {@code GloeWebSetup} injector
	 */
	public static Injector get() {
		if (injector == null)
			injector = new GloeWebSetup().createInjectorAndDoEMFRegistration();
		
		return injector;
	}

}
