package ice.master.loe.rest;

public enum Errors {
	
	SUCCESS(200, "success"),
	
	EMPTY_REQUEST(450, "the request is empty"),
	
	MALFORMED_REQUEST(451, "no viable alternative at input '<EOF>'"),
	
	INTERNAL_PARSING_ERROR(452, "an internal error occured while parsing the request"),
	
	INTERNAL_PROCESSING_ERROR(453, "an internal error occured while processing"),
	
	BOOKMARK_DOES_NOT_EXIST(454, "the bookmark does not exist"),
	
	INCORRECT_REQUEST(455, "the request has errors"),
	
	SUMMONER_DOES_NOT_EXIST(456, "the summoner does not exist");
	
	public final int code;
	
	public final String message;
	
	private Errors(int code, String message) {
		this.code = code;
		this.message = message;
	}
	
	public String asJSON() {
		return asJSON(code, message);
	}
	
	public String asJSON(String message) {
		return asJSON(this.code, message);
	}

	public static String asJSON(int code, String message) {
		return "{"
     + 		"\"code\": " + code + ","
     +		"\"message\": \"" + message + "\""
	 + "}";
	}
}
