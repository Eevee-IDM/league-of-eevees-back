package ice.master.loe.rest.exceptions;

/**
 * Thrown when an element was expected to be in a collection, but it was not.
 * 
 * @author Emmanuel Chebbi
 */
public class ElementNotFoundException extends RuntimeException {

	/**
	 * Generated serial UID
	 */
	private static final long serialVersionUID = 1622039980020843372L;

	public ElementNotFoundException() {
		super();
	}

	public ElementNotFoundException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public ElementNotFoundException(String arg0) {
		super(arg0);
	}

	public ElementNotFoundException(Throwable arg0) {
		super(arg0);
	}

}
