/**
 * Defines exceptions tailored to deal with errors that occurs during user requests processing.
 * 
 * @author Emmanuel CHEBBI
 */
package ice.master.loe.rest.exceptions;