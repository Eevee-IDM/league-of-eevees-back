package ice.master.loe.rest.exceptions;

/**
 * Thrown when one uses a game id that corresponds to no existing game.
 * 
 * @author Emmanuel CHEBBI
 */
public class GameNotFoundException extends RuntimeException {

	/**
	 * Serial UID
	 */
	private static final long serialVersionUID = 9135421411729775027L;

	public GameNotFoundException(String message) {
		super(message);
	}

	public GameNotFoundException(Throwable cause) {
		super(cause);
	}

	public GameNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

}
