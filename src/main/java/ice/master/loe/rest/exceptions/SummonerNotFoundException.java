package ice.master.loe.rest.exceptions;

import ice.master.loe.model.loe.Request;
import ice.master.loe.model.loe.Summoner;

/**
 * Thrown when a {@link Request} specifies a {@link Summoner} that does not exist in LoL.
 * 
 * @author Emmanuel CHEBBI
 */
public class SummonerNotFoundException extends RuntimeException {

	/**
	 * Generated serial UID
	 */
	private static final long serialVersionUID = -7420129254100846661L;

	public SummonerNotFoundException(String message) {
		super(message);
	}

	public SummonerNotFoundException(Throwable cause) {
		super(cause);
	}

	public SummonerNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

}
