package ice.master.loe.rest;

import java.io.IOException;
import java.net.URI;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.servlet.ServletRegistration;
import org.glassfish.grizzly.servlet.WebappContext;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import com.sun.jersey.api.core.ClassNamesResourceConfig;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.spi.container.servlet.ServletContainer;

import ice.master.loe.rest.cors.CORSFilter;
import ice.master.loe.rest.gloe.GloeServlet;

/**
 * Main class.
 *
 */
public class Main {
	// Base URI the Grizzly HTTP server will listen on
	public static final String BASE_URI = "http://0.0.0.0:5150/"; 

	private static final String JERSEY_SERVLET_CONTEXT_PATH = "";

	/**
	 * Starts Grizzly HTTP server exposing JAX-RS resources defined in this
	 * application.
	 * 
	 * @return Grizzly HTTP server.
	 */
	public static HttpServer startServer(String url) {
		// create a resource config that scans for JAX-RS resources and providers
		// in ice.master.loe.rest package
		final ResourceConfig rc = new ResourceConfig().packages("ice.master.loe.rest");
		rc.register(CORSFilter.class);
		
		// create and start a new instance of grizzly http server
		// exposing the Jersey application at BASE_URI
		HttpServer server = GrizzlyHttpServerFactory.createHttpServer(URI.create(url), rc);
		return configuredWithXtextServlet(server);
	}

	/** 
	 * Adds {@link GloeServlet} to {@code server}.<br>
	 * <br>
	 * The {@code GloeServlet} makes possible to use Gloe grammar
	 * within a text-editor embedded by a front-end service, such as
	 * Ace or Orion.
	 * 
	 *  @param server
	 *  			The HTTP server that should embed {@code GloeServlet}.
	 *  
	 *  @return {@code server} to enable method chaining 
	 */
	private static HttpServer configuredWithXtextServlet(HttpServer server) {
		// Initialize and register Jersey Servlet
		WebappContext context = new WebappContext("WebappContext", JERSEY_SERVLET_CONTEXT_PATH);

		ServletRegistration registration = context.addServlet("ServletContainer", GloeServlet.class);
		registration.setInitParameter(ServletContainer.RESOURCE_CONFIG_CLASS, PackagesResourceConfig.class.getName());
		registration.setInitParameter(ClassNamesResourceConfig.PROPERTY_CLASSNAMES, GloeServlet.class.getName());
		registration.addMapping("xtext-service/*");
		context.deploy(server);

		return server;
	}

	/**
	 * Main method.
	 * 
	 * @param args
	 * @throws IOException
	 * @throws InterruptedException 
	 */
	public static void main(String[] args) throws InterruptedException {
		String url = BASE_URI;
			
		if (args.length > 0)
			url = args[0];
		
		final HttpServer server = startServer(url);
		
		Thread.currentThread().join();
		server.shutdownNow();
	}
}
