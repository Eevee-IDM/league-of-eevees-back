package ice.master.loe.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import ice.master.loe.model.loe.Request;
import ice.master.loe.rest.gloe.GloeParser;
import ice.master.loe.rest.processing.RequestProcess;
import ice.master.loe.util.Try;
import lombok.extern.log4j.Log4j2;

/**
 * REST service parsing Gloe grammar.
 * 
 * @author Emmanuel CHEBBI
 */
@Log4j2
@Path("gloe")
public class GloeEndpoint {
	
    /**
     * Method handling HTTP GET requests. The returned object will be sent
     * to the client as "text/plain" media type.
     *
     * @return String that will be returned as a text/plain response.
     */
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String parse(@QueryParam("query") String query) {
    	log.info("Receiving: " + query);
    	
    	Try<Request> request = new GloeParser().parse(query);
    	
    	if (request.isFailure()) {
    		log.warn("Unable to parse " + query);
    		return request.error();
    	}
		
		RequestProcess traitement = new RequestProcess(request.get());
		try {
			String result = traitement.getResult();
			
			log.info("Returning " + result);
			return result;
			
		} catch (Exception e) {
			log.error("An error occured while processing user's request", e);
			return Errors.INTERNAL_PROCESSING_ERROR.asJSON();
		}
    }
    
}
