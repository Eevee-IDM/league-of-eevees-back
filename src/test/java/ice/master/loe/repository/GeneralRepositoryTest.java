package ice.master.loe.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import com.google.gson.JsonObject;

import ice.master.loe.TestUtils;

/**
 * Tests the behaviour of a {@link GeneralRepository}.
 * 
 * @author Léo Calvis
 * @author Emmanuel Chebbi
 */
@DisplayName("A General Repository")
public class GeneralRepositoryTest implements TestUtils {
	
	private static GeneralRepository generalRepo = GeneralRepository.REPO;
	
	private static final String SARENYA_ID = "28483640";
	private static final String MATCHES = "matches";
	private static final String TOTALGAMES = "totalGames";

	@ParameterizedTest
	@CsvSource({"Sarénya, " + SARENYA_ID})
	@DisplayName("can retrieve an account ID from the summoner name")
	public void canRetrieveAnAccountIdFromTheSummonerName(String summoner, String expectedAccountId) {
		assertThat(generalRepo.getAccountId(summoner)).contains(expectedAccountId);
	}

	@Test @DisplayName("can retrieve all the games played within a given range of time")
	public void canRetrieveAllTheGamesPlayedWithinAGivenRangeOfTime() throws IOException {
		JsonObject result = generalRepo.getHistoric(SARENYA_ID, 1498686931000L, 1501278931000L, 4, 112).getAsJsonObject();
		JsonObject expected = getJsonObject("ritoTestGetHistoricStringLongLongIntIntCollectionOfIntegerCollectionOfIntegerCollectionOfInteger.json");
		
		SoftAssertions softly = new SoftAssertions();

		softly.assertThat(result.get(MATCHES)).isEqualTo(expected.get(MATCHES));
		softly.assertThat(result.get(TOTALGAMES)).isEqualTo(expected.get(TOTALGAMES));
		
		softly.assertAll();
	}

	@ParameterizedTest
	@CsvSource({"3586682219, ritoTestGameStats.json"})
	@DisplayName("can retrieve all the statistics associated with a specific game")
	public void canRetrieveAllTheStatisticsAssociatedWithASpecificGame(String gameId, String expectedJsonFile) throws IOException {
		JsonObject result = generalRepo.getGameStats(gameId).getAsJsonObject();
		assertThat(result).isEqualTo(getJsonObject(expectedJsonFile));
	}

	@ParameterizedTest
	@CsvSource({"3586682219, ritoTestGameTimeline.json"})
	@DisplayName("can retrieve the whole timeline of a specific game")
	public void canRetrieveTheWholeTimelineOfASpecificGame(String gameId, String expectedJsonFile) throws IOException {
		JsonObject result = generalRepo.getGameTimeline(gameId).getAsJsonObject();
		assertThat(result).isEqualTo(getJsonObject(expectedJsonFile));
	}

}
