package ice.master.loe.repository.elastic;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;

import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import com.google.gson.JsonObject;

import ice.master.loe.TestUtils;
import io.searchbox.client.JestClient;
import io.searchbox.core.Delete;
import io.searchbox.indices.DeleteIndex;
import io.searchbox.indices.Refresh;

/**
 * Tests the behaviour of {@link ElasticsearchRepository}.
 * 
 * @author François-Marie d'Aboville
 * @author Emmanuel Chebbi
 */
@DisplayName("An ElasticSearch-based repository")
public class ElasticsearchRepositoryTest implements TestUtils {

	private static final String ACCOUNT_ID = "1010101";
	private static final String GAME_ID = "1000001";
	private static final String MATCHES = "matches";
	private static final String TOTAL_GAMES = "totalGames";
	private static final String CHAMPION = "champion";
	private static final String QUEUE = "queue";
	private static final String SEASON = "season";
	private static final String ROLE = "role";
	private static final String LANE = "lane";
	private static final String DUO_SUPPORT = "DUO_SUPPORT";

	/** Used to initialise and clear the database */
	private static JestClient client = ElasticsearchManager.client;

	private ElasticsearchRepository elasticRepo;

	@BeforeEach
	public void setUp() throws IOException {
		elasticRepo = new ElasticsearchRepository();
		elasticRepo.setHistoric(this.getJsonObject("historic.json"), ACCOUNT_ID);
		elasticRepo.setGameStats(this.getJsonObject("match.json"));
		elasticRepo.setGameTimeline(this.getJsonObject("timeline.json"), GAME_ID);
	}

	@AfterEach
	public void tearDown() throws IOException {
		client.execute(new DeleteIndex.Builder(ACCOUNT_ID).build());
		client.execute(new Delete.Builder(GAME_ID).index(MATCHES).type("game").build());
		client.execute(new Delete.Builder(GAME_ID).index("timelines").type("game").build());
	}

	@AfterAll
	public static void afterClass() throws IOException {
		Refresh refresh = new Refresh.Builder().build();
		client.execute(refresh);
	}

	@Nested
	@DisplayName("When asked for an existing game")
	class WhenAskedForAnExistingGame {

		@Test
		@DisplayName("can filter games with a range of time, in a given pagination")
		public void canFilterGamesWithTimeRangeInAPagination() throws IOException {
			JsonObject result = elasticRepo.getHistoric(ACCOUNT_ID, 1520728715862L, 1522552238313L, 0, 4);

			SoftAssertions softly = new SoftAssertions();

			softly.assertThat(result.get(TOTAL_GAMES).getAsInt()).isEqualTo(2);
			softly.assertThat(result.get(MATCHES).getAsJsonArray().get(0).getAsJsonObject().get(CHAMPION).getAsInt()).isEqualTo(99);
			softly.assertThat(result.get(MATCHES).getAsJsonArray().get(0).getAsJsonObject().get(QUEUE).getAsInt()).isEqualTo(450);
			softly.assertThat(result.get(MATCHES).getAsJsonArray().get(0).getAsJsonObject().get(SEASON).getAsInt()).isEqualTo(11);
			softly.assertThat(result.get(MATCHES).getAsJsonArray().get(0).getAsJsonObject().get(ROLE).getAsString()).isEqualTo(DUO_SUPPORT);
			softly.assertThat(result.get(MATCHES).getAsJsonArray().get(0).getAsJsonObject().get(LANE).getAsString()).isEqualTo("MID");

			softly.assertAll();
		}

		@Test
		@DisplayName("can retrieve all games in a pagination")
		public void canRetrieveAllGamesInAPagination() throws IOException {
			JsonObject result = elasticRepo.getHistoric(ACCOUNT_ID, 2, 4);

			SoftAssertions softly = new SoftAssertions();

			softly.assertThat(result.get(TOTAL_GAMES).getAsInt()).isEqualTo(100);
			softly.assertThat(result.get(MATCHES).getAsJsonArray().get(1).getAsJsonObject().get(CHAMPION).getAsInt()).isEqualTo(223);
			softly.assertThat(result.get(MATCHES).getAsJsonArray().get(1).getAsJsonObject().get(QUEUE).getAsInt()).isEqualTo(450);
			softly.assertThat(result.get(MATCHES).getAsJsonArray().get(1).getAsJsonObject().get(SEASON).getAsInt()).isEqualTo(11);
			softly.assertThat(result.get(MATCHES).getAsJsonArray().get(1).getAsJsonObject().get(ROLE).getAsString()).isEqualTo("SOLO");
			softly.assertThat(result.get(MATCHES).getAsJsonArray().get(1).getAsJsonObject().get(LANE).getAsString()).isEqualTo("TOP");

			softly.assertAll();
		}

		@Test
		@DisplayName("can retrieve all the stats of a specific game")
		public void canRetrieveAllTheStatsOfASpecificGame() throws IOException {
			JsonObject result = elasticRepo.getGameStats(GAME_ID);

			SoftAssertions softly = new SoftAssertions();

			softly.assertThat(result.get("teams").isJsonArray()).isTrue();
			softly.assertThat(result.get("participants").isJsonArray()).isTrue();
			softly.assertThat(result.get("participantIdentities").isJsonArray()).isTrue();

			softly.assertAll();
		}

		@Test
		@DisplayName("can retrieve the whole timeline of a specific game")
		public void canRetrieveTheWholeTimelineOfASpecificGame() throws IOException {
			JsonObject result = elasticRepo.getGameTimeline(GAME_ID);

			SoftAssertions softly = new SoftAssertions();

			softly.assertThat(result.get("frames").isJsonArray()).isTrue();
			softly.assertThat(result.get("frames").getAsJsonArray().size()).isEqualTo(12);

			softly.assertAll();
		}

		@ParameterizedTest(name = "{0}, should exist={1}")
		@CsvSource({ ACCOUNT_ID + ", true", "non-existing account,  false", "testpasserajamais, false" })
		@DisplayName("can say whether an historic exists for a given account")
		public void canSayWhetherAnHistoricExistsForAGivenAccount(String accountId, boolean shouldExist)
				throws IOException {
			assertThat(elasticRepo.hasHistoricOf(accountId)).isEqualTo(shouldExist);
		}

		@ParameterizedTest(name = "{0}, should be stored={1}")
		@CsvSource({ GAME_ID + ", true", "wronggameid,  false" })
		@DisplayName("can say whether stats are stored for a given game")
		public void canSayWhetherStatsAreStoredForAGivenGame(String gameId, boolean shouldExist) throws IOException {
			assertThat(elasticRepo.hasStatisticsOf(gameId)).isEqualTo(shouldExist);
		}

		@ParameterizedTest(name = "{0}, should be stored={1}")
		@CsvSource({ GAME_ID + ", true", "wronggameid,  false" })
		@DisplayName("can say whether a timeline is stored for a given game")
		public void canSayWhetherATimelineIsStoredForAGivenGame(String gameId, boolean shouldExist) throws IOException {
			assertThat(elasticRepo.hasTimelineOf(gameId)).isEqualTo(shouldExist);
		}
	}

	@Nested
	@DisplayName("When asked for a non existing game")
	class WhenAskedForANonExistingGame {

		@Test
		@DisplayName("returns null when asked for the game's stats")
		public void returnsNullWhenAskedForTheStatsOfANonExistingGame() throws IOException {
			assertThat(elasticRepo.getGameStats("i do not exist")).isNull();
		}

		@Test
		@DisplayName("returns null when asked for the game's timeline")
		public void returnsNullWhenAskedForTheTimelineOfANullGame() throws IOException {
			assertThat(elasticRepo.getGameTimeline("-54656565656565")).isNull();
		}

		@Test
		@DisplayName("does not store the game's stats")
		public void returnsFalseWhenAskedWhetherTheStatsOfANullGameAreStored() throws IOException {
			assertThat(elasticRepo.hasStatisticsOf("me neither")).isFalse();
		}

		@Test
		@DisplayName("does not contain the game's timeline")
		public void returnsFalseWhenAskedWhetherTheTimelineOfANullGameIsStored() throws IOException {
			assertThat(elasticRepo.hasTimelineOf("# ?")).isFalse();
		}
	}

}
