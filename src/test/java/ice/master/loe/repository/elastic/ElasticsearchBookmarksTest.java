package ice.master.loe.repository.elastic;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import ice.master.loe.TestUtils;

/**
 * Tests the behaviour of a {@link ElasticsearchBookmarks}.
 * 
 * @author François-Marie d'Aboville
 * @author Emmanuel Chebbi
 */
@DisplayName("A Bookmarks Database")
public class ElasticsearchBookmarksTest implements TestUtils {

	private static final String ACCOUNT_ID = "219355319"; // comes from sample.json
	private static final String ID = "someID";
	private static final String BOOKMARKS_SAMPLE_JSON = "bookmarks/sample.json";
	
	@Nested @DisplayName("When empty")
	class WhenEmpty {
		
		private ElasticsearchBookmarks db;
		
		@BeforeEach()
		public void createDatabase() throws IOException {
			db = new ElasticsearchBookmarks();
		}
		
		@AfterEach
		public void deleteNewBookmarks() throws IOException {
			db.deleteBookmark(ID);
		}

		@Test @DisplayName("returns an empty array when asked for a user's bookmarks")
		void returnsAnEmptyArrayWhenAskedForAUserBookmarks() throws IOException {
			assertThat(db.getAllBookmarks(ACCOUNT_ID)).isEmpty();
		}
		
		@Test @DisplayName("returns an empty optional when asked for a bookmark")
		void returnsAnEmptyOptionalWhenAskedForABookmark() throws IOException {
			assertThat(db.getBookmark(ID)).isEmpty();
		}
		
		@Test @DisplayName("returns false trying to delete a bookmark")
		void returnsFalseWhenTryingToDeleteABookmark() throws IOException {
			assertThat(db.deleteBookmark(ID)).isFalse();
		}
		
		@Test @DisplayName("can create a new bookmark then retrieve it")
		void canCreateANewBookmarkThenRetrieveIt() throws IOException {
			JsonObject request = getJsonObject(BOOKMARKS_SAMPLE_JSON);
			db.createBookmark(ID, request);
			
			assertThat(db.getBookmark(ID)).contains(request);
		}
		
		@Test @DisplayName("replaces a bookmark when creating a new one with identical ID") 
		void cannotCreateTwoBookmarksWithTheSameId() throws IOException {
			db.createBookmark(ID, getJsonObject(BOOKMARKS_SAMPLE_JSON));
			db.createBookmark(ID, getJsonObject("bookmarks/sample2.json"));
			
			JsonArray expectedBookmarks = new JsonArray();
			expectedBookmarks.add(getJsonObject("bookmarks/sample2.json"));

			assertThat(db.getAllBookmarks(ACCOUNT_ID)).isEqualTo(expectedBookmarks);
		}
	}
	
	@Nested @DisplayName("When not empty")
	class WhenNotEmpty {
		
		private ElasticsearchBookmarks db;
		
		@BeforeEach()
		public void createDatabase() throws IOException {
			db = new ElasticsearchBookmarks();
			db.createBookmark(ID, getJsonObject(BOOKMARKS_SAMPLE_JSON));
		}
		
		@AfterEach
		public void deleteNewBookmarks() throws IOException {
				db.deleteBookmark(ID);
		}
		
		@Test @DisplayName("returns true when deleting a bookmark")
		void returnsTrueWhenDeletingABookmark() throws IOException {
			assertThat(db.deleteBookmark(ID)).isTrue();
		}
		
		@Test @DisplayName("can delete a bookmark")
		void canDeleteABookmark() throws IOException {
			db.deleteBookmark(ID);
			assertThat(db.getAllBookmarks(ID)).isEmpty();
		}
	}
}
