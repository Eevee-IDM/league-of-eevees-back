package ice.master.loe.rest.bookmarks;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ice.master.loe.TestUtils;
import ice.master.loe.repository.elastic.ElasticsearchBookmarks;
import ice.master.loe.rest.Errors;

/**
 * Tests the behaviour of a {@link BookmarksEndpoint}.
 * 
 * @author Emmanuel Chebbi
 */
@DisplayName("A Bookmarks Endpoint")
public class BookmarksEndpointTest implements TestUtils {
	
	private static final String BOOKMARK_ID = "someID";
	private static final String SUMMONER_NAME = "Sin Lee";
	private static final String TITLE = "Some title";
	private static final String QUERY = "summoners '" + SUMMONER_NAME + "' display total of victories from '01 01 2018' to '01 02 2018' in a bar chart called '" + TITLE + "'";
	
	private static final String BOOKMARK_ID_2 = "anotherID";
	private static final String INVALID_QUERY = "not a valid query";
	
	private BookmarksEndpoint endpoint;
	
	private static final JsonParser json = new JsonParser();
	
	private static final ElasticsearchBookmarks db = new ElasticsearchBookmarks();
	
	@BeforeEach
	void createEndpoint() {
		endpoint = new BookmarksEndpoint();
	}
	
	@AfterEach
	public void deleteNewBookmarks() throws IOException {
		for (String id : new String[] { BOOKMARK_ID, BOOKMARK_ID_2 })
			db.deleteBookmark(id);
	}
	
	// getAll

	@Test @DisplayName("returns an empty JSON when the user has no bookmark")
	void returnsAnEmptyJsonWhenTheUserHasNoBookmark() throws JsonSyntaxException, FileNotFoundException {
		assertThat(json.parse(endpoint.getAllFavorites(SUMMONER_NAME)))
				.isEqualTo(getJsonObject("bookmarks/noBookmark.json"));
	}

	@Test @DisplayName("returns all the bookmarks of a given user")
	void returnsAllTheBookmarksOfAGivenUser() throws JsonSyntaxException, FileNotFoundException {
		endpoint.createFavorite(BOOKMARK_ID, SUMMONER_NAME, TITLE, QUERY);
		endpoint.createFavorite(BOOKMARK_ID_2, SUMMONER_NAME, TITLE, INVALID_QUERY);
		
		assertThat(json.parse(endpoint.getAllFavorites(SUMMONER_NAME)))
				.isEqualTo(getJsonObject("bookmarks/SinLee.json"));
	}
	
	// snapshot
	
	@Test @DisplayName("returns an empty JSON snapshot when the user has no bookmark")
	void returnsAnEmptyJsonSnapshotWhenTheUserHasNoBookmark() throws JsonSyntaxException, FileNotFoundException {
		assertThat(json.parse(endpoint.getSnapshot(SUMMONER_NAME)))
				.isEqualTo(getJsonObject("bookmarks/noBookmark.json"));
	}

	@Test @DisplayName("returns a snapshot of all the bookmarks of a given user")
	void returnsASnapshotOfTheBookmarksOfAGivenUser() throws JsonSyntaxException, FileNotFoundException {
		endpoint.createFavorite(BOOKMARK_ID, SUMMONER_NAME, TITLE, QUERY);
		endpoint.createFavorite(BOOKMARK_ID_2, SUMMONER_NAME, TITLE, INVALID_QUERY);
		
		assertThat(json.parse(endpoint.getSnapshot(SUMMONER_NAME)))
				.isEqualTo(getJsonObject("bookmarks/SinLeeSnapshot.json"));
	}
	
	// create
	
	@Test @DisplayName("can create a new bookmark")
	void allowsToCreateANewBookmark() throws JsonSyntaxException, FileNotFoundException {
		String result = endpoint.createFavorite(BOOKMARK_ID, SUMMONER_NAME, TITLE, QUERY);
		assertThat(json.parse(result)).isEqualTo(getJsonObject("results/bookmarkCreated.json"));
	}
	
	
	@ParameterizedTest
	@ValueSource(strings = {"i_do_not_exist_exist_not_do_i", "kjdhgjdgmqjd sjhfg"})
	@DisplayName("cannot create a new bookmark for a non-existing summoner")
	void cannotCreateANewBookmarkForANonExistingSummoner(String summoner) throws JsonSyntaxException, FileNotFoundException {
		String result = endpoint.createFavorite(BOOKMARK_ID, summoner, TITLE, QUERY);
		String expectedResult = Errors.SUMMONER_DOES_NOT_EXIST.asJSON("The summoner " + summoner + " does not exist");
		
		assertThat(json.parse(result)).isEqualTo(json.parse(expectedResult));
	}
	
	@Test @DisplayName("can create a bookmark with an invalid query")
	void canCreateABookMarkWithAnInvalidQuery() throws JsonSyntaxException, FileNotFoundException {
		String result = endpoint.createFavorite(BOOKMARK_ID_2, SUMMONER_NAME, TITLE, INVALID_QUERY);
		assertThat(json.parse(result)).isEqualTo(getJsonObject("results/bookmarkCreated.json"));
	}
	
	// get
	
	@Test @DisplayName("can retrieve an existing bookmark")
	void canRetrieveAnExistingBookmark() throws IOException {
		db.createBookmark(BOOKMARK_ID, getJsonObject("bookmarks/sample.json"));
		
		String result = endpoint.getFavorite(BOOKMARK_ID);
		assertThat(json.parse(result)).isEqualTo(getJsonObject("results/getSample.json"));
	}
	
	@Test @DisplayName("cannot retrieve a non-existing bookmark")
	void cannotRetrieveAnNonExistingBookmark() throws IOException {
		String result = endpoint.getFavorite("nonExistingID");
		assertThat(json.parse(result)).isEqualTo(getJsonObject("results/getNonExistingBookmark.json"));
	}
	
	// delete
	
	@Test @DisplayName("can delete an existing bookmark")
	void canDeleteAnExistingBookmark() throws IOException {
		db.createBookmark(BOOKMARK_ID, getJsonObject("bookmarks/sample.json"));
		
		String result = endpoint.deleteFromFavorites(BOOKMARK_ID);
		assertThat(json.parse(result)).isEqualTo(getJsonObject("results/success.json"));
	}
	
	@Test @DisplayName("can delete a non existing bookmark")
	void canDeleteANonExistingBookmark() throws IOException {
		String result = endpoint.deleteFromFavorites("nonExistingID");
		assertThat(json.parse(result)).isEqualTo(getJsonObject("results/success.json"));
	}
}