package ice.master.loe.rest;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.FileNotFoundException;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import ice.master.loe.TestUtils;

/**
 * Tests the behaviour of a {@link GloeEndpoint} instance.
 * 
 * @author Emmanuel CHEBBI
 */
@DisplayName("A Gloe Endpoint")
public class GloeEndpointTest implements TestUtils {
	
	private GloeEndpoint endpoint;
	
	private final JsonParser json = new JsonParser();
	
	@BeforeEach
	void createEndpoint() {
		endpoint = new GloeEndpoint();
	}
	
	@Test @DisplayName("handles null requests")
	void handlesNullRequests() throws JsonSyntaxException, FileNotFoundException {
		assertThat(json.parse(endpoint.parse(null))).isEqualTo(getJsonObject("results/emptyRequest.json"));
	}
	
	@ParameterizedTest
	@ValueSource(strings = {"", "  "})
	@DisplayName("handles empty requests")
	void handlesEmptyRequests(String request) throws FileNotFoundException {
		assertThat(json.parse(endpoint.parse(request))).isEqualTo(getJsonObject("results/emptyRequest.json"));
	}
	
	@ParameterizedTest
	@MethodSource("malformedRequests")
	@DisplayName("handles malformed requests")
	void handlesBrokenRequests(String request, String expectedResult) throws FileNotFoundException {
		assertThat(json.parse(endpoint.parse(request))).isEqualTo(json.parse(expectedResult));
	}
	
	/** @return a Stream of tuples (malformed request ; expected error message) */
	static Stream<Arguments> malformedRequests() {
		return Stream.of(
			Arguments.of("summoners 'Sarénya'", "{\"code\": 451,\"message\": \"mismatched input '<EOF>' expecting 'display'\"}"),
			Arguments.of("summoners 'Sarénya' display total of victories", "{\"code\": 451,\"message\": \"mismatched input '<EOF>' expecting 'in'\"}"),
			Arguments.of("summoners 'Sarénya' display total of victories in a star", "{\"code\": 451,\"message\": \"no viable alternative at input 'star'\"}"),
			Arguments.of("summoners 'Sarénya' in a bar chart", "{\"code\": 451,\"message\": \"mismatched input 'in' expecting 'display'\"}"),
			Arguments.of("display total of victories in a bar chart", "{\"code\": 451,\"message\": \"missing EOF at 'display'\"}")
		);
	}
	
	@Tag("expensive")
	@ParameterizedTest
	@MethodSource("goodRequests")
	@DisplayName("handles good requests")
	void handlesGoodRequests(String request, JsonElement expectedResult) {
		String jsonResult = endpoint.parse(request);
		
		JsonObject result = new JsonParser().parse(jsonResult).getAsJsonObject();
		result.remove("id");
		
		assertThat(result).isEqualTo(expectedResult);
	}
	
	/** @return a Stream of tuples (well formatted requests ; expected result) */
	static Stream<Arguments> goodRequests() {
		return Stream.of(
			Arguments.of("summoners 'Sarénya' display total of victories from '01 01 2017' to '01 01 2018' in a pie chart", new JsonParser().parse("{\"code\":200,\"data\":[{\"summoner\":\"Sarénya\",\"courbeName\":\"noSense\",\"courbeSummoner\":[{\"label\":\"victories\",\"value\":388}]}],\"indices\":[\"victories\"],\"date\":{\"from\":\"\",\"to\":\"\"},\"display\":{\"name\":\"null\",\"type\":\"pie2d\"}}")),
			Arguments.of("summoners 'Sarénya' display total of defeats from '01 01 2017' to '01 01 2018' in a  bar chart", new JsonParser().parse("{\"code\":200,\"data\":[{\"summoner\":\"Sarénya\",\"courbeName\":\"noSense\",\"courbeSummoner\":[{\"label\":\"defeats\",\"value\":388}]}],\"indices\":[\"defeats\"],\"date\":{\"from\":\"\",\"to\":\"\"},\"display\":{\"name\":\"null\",\"type\":\"column2d\"}}")),
			Arguments.of("summoners 'Sarénya' display total of victories when team is red from '01 01 2017' to '01 01 2018' in a pie chart", new JsonParser().parse("{\"code\":200,\"data\":[{\"summoner\":\"Sarénya\",\"courbeName\":\"noSense\",\"courbeSummoner\":[{\"label\":\"victories\",\"value\":202}]}],\"indices\":[\"victories\"],\"date\":{\"from\":\"\",\"to\":\"\"},\"display\":{\"name\":\"null\",\"type\":\"pie2d\"}}"))
		);
	}
}
