package ice.master.loe.rest;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.internal.MapPropertiesDelegate;
import org.glassfish.jersey.server.ContainerRequest;
import org.glassfish.jersey.server.ContainerResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import ice.master.loe.rest.cors.CORSFilter;

/**
 * Tests the behaviour of a CORSFilter instance.
 * 
 * @author Emmanuel Chebbi
 */
@DisplayName("A CORS Filter")
public class CORSFilterTest {

	private CORSFilter filter = new CORSFilter();
	private ContainerRequest request;
	private ContainerResponseContext response;
	private Map<String,List<Object>> expectedHeaders;
	
	@BeforeEach
	void initializeContext() {
		request = new ContainerRequest(URI.create("uri"), URI.create("uri"), "GET", null, new MapPropertiesDelegate());
		response = new ContainerResponse(request, Response.accepted().build());
		
		expectedHeaders = new HashMap<>();
		expectedHeaders.put("Access-Control-Allow-Origin", asList("*"));
		expectedHeaders.put("Access-Control-Allow-Headers", asList("origin, content-type, accept, authorization"));
		expectedHeaders.put("Access-Control-Allow-Credentials", asList("true"));
		expectedHeaders.put("Access-Control-Allow-Methods", asList("GET, POST, PUT, DELETE, OPTIONS, HEAD"));
	}

	@Test @DisplayName("adds Access-Control-Allow-Origin header")
	void addsAccessControlAllowOriginHeader() throws IOException {
		filter.filter(request, response);
		assertThat(response.getHeaders()).containsAllEntriesOf(expectedHeaders);
	}
	
}
