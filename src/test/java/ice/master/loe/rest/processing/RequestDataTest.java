package ice.master.loe.rest.processing;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import com.google.gson.JsonArray;

import ice.master.loe.TestUtils;
import ice.master.loe.model.loe.Champion;
import ice.master.loe.model.loe.ChampionValue;
import ice.master.loe.model.loe.Lane;
import ice.master.loe.model.loe.LaneValue;
import ice.master.loe.model.loe.LoeFactory;
import ice.master.loe.model.loe.Summoner;
import ice.master.loe.model.loe.Timelapse;
import ice.master.loe.rest.exceptions.GameNotFoundException;
import ice.master.loe.rest.exceptions.SummonerNotFoundException;

/**
 * Tests the behaviour of a {@link RequestData} instance.
 * 
 * @author Emmanuel CHEBBI
 */
@DisplayName("A Request Data")
public class RequestDataTest implements TestUtils {
	
	private static final String SARENYA_NAME = "Sarénya";
	private static final String SARENYA_ID = "28483640";
	private static final String SARENYA_GAME_ID = "3586682219";
	private static final String NON_EXISTING_GAME_ID = "some non existing game id";
	
	private final Summoner invalidSummoner;
	
	public RequestDataTest() {
		invalidSummoner = LoeFactory.eINSTANCE.createSummoner();
		invalidSummoner.setName("aaabaaabaaabab");
	}
	
	@Test @DisplayName("throws when initialized with a non-existing summoner")
	void throwsWhenInitializedWithANonExistingSummoner() {
		assertThatExceptionOfType(SummonerNotFoundException.class)
			.isThrownBy(() -> new RequestData(asList(invalidSummoner), LoeFactory.eINSTANCE.createTimelapse()));
	}
	
	@Nested @DisplayName("When initialized without any summoner")
	class WhenInitializedWithoutAnySummoner {
		
		private RequestData data;
		
		@BeforeEach
		void initializeData() throws IOException {
			data = new RequestData(new ArrayList<>(), null);
		}
		
		@Test @DisplayName("has 0 summoners")
		void has0Summoners() {
			assertThat(data.numberOfSummoners()).isEqualTo(0);
		}
		
		@Test @DisplayName("throws when asked for a summoner's id")
		void throwsWhenAskedForASummonerId() {
			assertThatExceptionOfType(SummonerNotFoundException.class)
				.isThrownBy(() -> data.getSummonerId(SARENYA_NAME));
		}
		
		@Test @DisplayName("throws when asked for a summoner's number of games")
		void throwsWhenAskedForTheNumberOfGamesPlayedByASummoner() {
			assertThatExceptionOfType(SummonerNotFoundException.class)
				.isThrownBy(() -> data.numberOfGames(SARENYA_ID));
		}
		
		@Test @DisplayName("throws when asked for a summoner's historic")
		void throwsWhenAskedForASummonerHistoric() {
			assertThatExceptionOfType(SummonerNotFoundException.class)
				.isThrownBy(() -> data.getHistoric(SARENYA_ID));
		}
		
		@Test @DisplayName("throws when asked for a game's statistics")
		void throwsWhenAskedForAGameStatistics() {
			assertThatExceptionOfType(GameNotFoundException.class)
				.isThrownBy(() -> data.getGameStats(NON_EXISTING_GAME_ID));
		}
		
		@Test @DisplayName("throws when asked for a game's timeline")
		void throwsWhenAskedForAGameTimeline() {
			assertThatExceptionOfType(GameNotFoundException.class)
				.isThrownBy(() -> data.getGameTimeline(NON_EXISTING_GAME_ID));
		}
	}

	@Nested @DisplayName("When initialized with a summoner and empty filters")
	class WhenInitializedWithASummonerAndEmptyFilters {
		
		private RequestData data;
		
		@BeforeEach
		void initializeData() throws IOException {
			final Summoner sarenya = LoeFactory.eINSTANCE.createSummoner();
			sarenya.setName(SARENYA_NAME);
			
			final Timelapse timelapse = LoeFactory.eINSTANCE.createTimelapse();
			timelapse.setFrom(new GregorianCalendar(2017, 01, 01).getTime());
			
			final Map<Summoner, RequestFilter> summonerToFilters = new HashMap<>();
			summonerToFilters.put(sarenya, new RequestFilter());
			
			data = new RequestData(asList(sarenya), null, summonerToFilters);
		}
		
		@Test @DisplayName("has 1 summoner")
		void has1Summoner() {
			assertThat(data.numberOfSummoners()).isEqualTo(1);
		}
		
		@Test @DisplayName("knows the summoner's id")
		void knowsSummonerId() {
			assertThat(data.getSummonerId(SARENYA_NAME)).isEqualTo(SARENYA_ID);
		}
		
		@Test @DisplayName("knows summoner's historic")
		void knowsSummonerHistoric() {
			assertThat(data.getHistoric(SARENYA_ID)).isInstanceOf(JsonArray.class);
		}
		
		@Test @DisplayName("knows its games' statistics")
		void knowsItsGamesStatistics() throws FileNotFoundException {
			assertThat(data.getGameStats(SARENYA_GAME_ID)).isEqualTo(getJsonObject("ritoTestGameStats.json"));
		}
		
		@Test @DisplayName("knows its games' timeline")
		void knowsItsGamesTimeline() throws IOException {
			assertThat(data.getGameTimeline(SARENYA_GAME_ID)).isEqualTo(getJsonObject("ritoTestGameTimeline.json"));
		}
	}
	
	@Nested @DisplayName("When initialized with a summoner and some filters")
	class WhenInitializedWithASummonerAndSomeFilters {
		
		private RequestData data;
		
		@BeforeEach
		void initializeData() throws IOException {
			final Summoner sarenya = LoeFactory.eINSTANCE.createSummoner();
			sarenya.setName(SARENYA_NAME);
			
			final Timelapse timelapse = LoeFactory.eINSTANCE.createTimelapse();
			timelapse.setFrom(new GregorianCalendar(2017, 01, 01).getTime());
			timelapse.setTo(new GregorianCalendar(2018, 01, 01).getTime());
			
			RequestFilter filters = new RequestFilter();
			filters.addChampion(valueOf(Champion.AHRI));
			filters.addChampion(valueOf(Champion.KATARINA));
			filters.addLane(valueOf(Lane.TOP));
			
			final Map<Summoner, RequestFilter> summonerToFilters = new HashMap<>();
			summonerToFilters.put(sarenya, filters);
			
			data = new RequestData(asList(sarenya), timelapse, summonerToFilters);
		}
	
		@Test @DisplayName("knows summoner's number of games")
		void knowsSummonerNumberOfGames() throws FileNotFoundException {
			assertThat(data.numberOfGames(SARENYA_ID)).isEqualTo(228);
		}
		
		private ChampionValue valueOf(Champion champion) {
			ChampionValue value = LoeFactory.eINSTANCE.createChampionValue();
			value.setChampion(champion);
			return value;
		}
		
		private LaneValue valueOf(Lane lane) {
			LaneValue value = LoeFactory.eINSTANCE.createLaneValue();
			value.setLane(lane);
			return value;
		}
	}
}
