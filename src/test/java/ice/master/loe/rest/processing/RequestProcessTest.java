package ice.master.loe.rest.processing;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNullPointerException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.stream.Stream;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.xtext.resource.XtextResourceSet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.inject.Injector;

import ice.master.loe.model.loe.Request;
import ice.master.loe.rest.gloe.GloeInjector;

/**
 * Tests the behaviour of a {@link RequestProcess} instance.
 * 
 * @author Emmanuel Chebbi
 */
@DisplayName("A Request Process")
public class RequestProcessTest {
	
	private static Request withoutFilters;
	private static Request filterChampionTeam;
	private static Request filterResultLane;
	private static Request nullTimeline;
	private static Request byYearWith2Summoners;
	private static Request byTeamWith3Summoners;
	private static Request byChampionWith3Summoners;
	private static Request byDayWith2SummonersAnd2Display;
	
	private static final JsonParser JSON = new JsonParser();
	
	static {
		withoutFilters = parseQuery(
			  "summoners 'Sarénya' "
			+ "display winrate, total of games in top "
			+ "from '01 10 2017' to '30 12 2017' "
			+ "in a pie chart "
		);
		filterChampionTeam = parseQuery(
			  "summoners 'Sarénya' "
			+ "display average of kills, total of games in red team "
			+ "when playing Katarina and team is blue "
			+ "from '01 01 2017' to '01 05 2017' "
			+ "in a bar chart called 'Lots of things'"
		);
		filterResultLane = parseQuery(
			  "summoners 'Sarénya' "
			+ "display total of kills, total of games with Katarina, total of games in red team "
			+ "when lane is mid and ends with victory "
			+ "from '01 01 2017' to '01 05 2017' "
			+ "in a bar chart called 'Lots of things' "
		);
		nullTimeline = parseQuery(
			  "summoners 'Sarénya' "
			+ "display total of defeats, total of victories, total of games in blue team "
			+ "when lane is top and ends with victory "
			+ "in a pie chart 3D called 'Defeats -- end of 2017' "
		);
		byYearWith2Summoners = parseQuery("summoners 'Sarénya', 'Thargen' "
				+ "display total of games with Katarina "
				+ "by year "
				+ "from '01 01 2016' to '01 06 2018' "
				+ "in a line chart"
		);
		byTeamWith3Summoners = parseQuery("summoners \"Sarénya\", \"Thargen\", \"Fulltenshu\" "
				+ "display average of kills "
				+ "by team "
				+ "from \"01 01 2016\" to \"01 06 2016\" "
				+ "in a bar chart"
		);
		byChampionWith3Summoners = parseQuery("summoners \"Sarénya\", \"Thargen\", \"Fulltenshu\" "
				+ "display average of kills "
				+ "by champion "
				+ "when playing Ezreal "
				+ "from \"01 01 2016\" to \"01 06 2016\" "
				+ "in a bar chart"
		);
		byDayWith2SummonersAnd2Display = parseQuery("summoners \"Sarénya\", \"Sobievsky\" "
				+ "display total of victories, total of defeats "
				+ "by day "
				+ "from \"01 01 2016\" to \"07 01 2016\" "
				+ "in a line chart"
		);
	}
	
	private RequestProcess process;
	
	@BeforeEach
	void createRequestProcess() {
		process = new RequestProcess(withoutFilters);
	}
	
	@Test @DisplayName("throws when initialized with a null request")
	void throwsWhenInitializedWithANullRequest() {
		assertThatNullPointerException().isThrownBy(() -> new RequestProcess(null));
	}
	
	@Test @DisplayName("handles requests that does not have a timeline") 
	void handlesRequestsThatDoesNotHaveATimeline() throws IOException {
		// since the result computes all the games until today, we cannot check the result
		new RequestProcess(nullTimeline).getResult();
	}
	
	@ParameterizedTest()
	@MethodSource("getQueriesAndExpectedResult")
	@DisplayName("interprets the query")
	void interpretsTheQuery(Request request, JsonElement expected) throws JsonSyntaxException, IOException {
		process = new RequestProcess(request);
		JsonObject response = JSON.parse(process.getResult()).getAsJsonObject();
		response.remove("id"); // The id depends on the timestamp, making it impossible to test here
		assertThat(response).isEqualTo(expected);
	}
	
	@ParameterizedTest()
	@MethodSource("getQueriesAndExpectedResult")
	@DisplayName("processes the query only once")
	void processesTheQueryOnlyOnce(Request request, JsonElement expected) throws IOException {
		process = new RequestProcess(request);
		String result = process.getResult();
		
		long start = System.currentTimeMillis();
		String result2 = process.getResult();
		long end = System.currentTimeMillis();
		
		assertThat(end - start).isLessThan(500);
		assertThat(result2).isEqualTo(result);
	}
	
	static Stream<Arguments> getQueriesAndExpectedResult() {
		return Stream.of(
			Arguments.of(withoutFilters, JSON.parse("{\"code\":200,\"data\":[{\"summoner\":\"Sarénya\",\"courbeName\":\"noSense\",\"courbeSummoner\":[{\"label\":\"victories\",\"value\":\"52.03%\"},{\"label\":\"top lane games\",\"value\":19}]}],\"indices\":[\"victories\",\"top lane games\"],\"date\":{\"from\":\"\",\"to\":\"\"},\"display\":{\"name\":\"null\",\"type\":\"pie2d\"}}")),
			Arguments.of(filterChampionTeam, JSON.parse("{\"code\":200,\"data\":[{\"summoner\":\"Sarénya\",\"courbeName\":\"noSense\",\"courbeSummoner\":[{\"label\":\"kills\",\"value\":12.38},{\"label\":\"red team games\",\"value\":0}]}],\"indices\":[\"kills\",\"red team games\"],\"date\":{\"from\":\"\",\"to\":\"\"},\"display\":{\"name\":\"Lots of things\",\"type\":\"column2d\"}}")),
			Arguments.of(filterResultLane, JSON.parse("{\"code\":200,\"data\":[{\"summoner\":\"Sarénya\",\"courbeName\":\"noSense\",\"courbeSummoner\":[{\"label\":\"kills\",\"value\":136},{\"label\":\"Katarina games\",\"value\":7},{\"label\":\"red team games\",\"value\":6}]}],\"indices\":[\"kills\",\"Katarina games\",\"red team games\"],\"date\":{\"from\":\"\",\"to\":\"\"},\"display\":{\"name\":\"Lots of things\",\"type\":\"column2d\"}}")),
			Arguments.of(byYearWith2Summoners, JSON.parse("{\"code\":200,\"data\":[{\"summoner\":\"Sarénya\",\"courbeName\":\"Katarina games\",\"courbeSummoner\":[{\"label\":\"2016\",\"value\":756},{\"label\":\"2017\",\"value\":528},{\"label\":\"2018\",\"value\":6}]},{\"summoner\":\"Thargen\",\"courbeName\":\"Katarina games\",\"courbeSummoner\":[{\"label\":\"2016\",\"value\":10},{\"label\":\"2017\",\"value\":2},{\"label\":\"2018\",\"value\":0}]}],\"indices\":[\"2016\",\"2017\",\"2018\"],\"date\":{\"from\":\"\",\"to\":\"\"},\"display\":{\"name\":\"null\",\"type\":\"line\"}}")),
			Arguments.of(byTeamWith3Summoners, JSON.parse("{\"code\":200,\"data\":[{\"summoner\":\"Sarénya\",\"courbeName\":\"noSense\",\"courbeSummoner\":[{\"label\":\"blue team kills\",\"value\":10.98},{\"label\":\"red team kills\",\"value\":11.21}]},{\"summoner\":\"Thargen\",\"courbeName\":\"noSense\",\"courbeSummoner\":[{\"label\":\"blue team kills\",\"value\":7.08},{\"label\":\"red team kills\",\"value\":7.64}]},{\"summoner\":\"Fulltenshu\",\"courbeName\":\"noSense\",\"courbeSummoner\":[{\"label\":\"blue team kills\",\"value\":8.39},{\"label\":\"red team kills\",\"value\":8.52}]}],\"indices\":[\"blue team kills\",\"red team kills\"],\"date\":{\"from\":\"\",\"to\":\"\"},\"display\":{\"name\":\"null\",\"type\":\"column2d\"}}")),
			Arguments.of(byChampionWith3Summoners, JSON.parse("{\"code\":200,\"data\":[{\"summoner\":\"Sarénya\",\"courbeName\":\"noSense\",\"courbeSummoner\":[{\"label\":\"Ezreal kills\",\"value\":12.50}]},{\"summoner\":\"Thargen\",\"courbeName\":\"noSense\",\"courbeSummoner\":[{\"label\":\"Ezreal kills\",\"value\":7.61}]},{\"summoner\":\"Fulltenshu\",\"courbeName\":\"noSense\",\"courbeSummoner\":[{\"label\":\"Ezreal kills\",\"value\":14.67}]}],\"indices\":[\"Ezreal kills\"],\"date\":{\"from\":\"\",\"to\":\"\"},\"display\":{\"name\":\"null\",\"type\":\"column2d\"}}")),
			Arguments.of(byDayWith2SummonersAnd2Display, JSON.parse("{\"code\":200,\"data\":[{\"summoner\":\"Sarénya\",\"courbeName\":\"victories\",\"courbeSummoner\":[{\"label\":\"2016-01-01\",\"value\":5},{\"label\":\"2016-01-02\",\"value\":5},{\"label\":\"2016-01-03\",\"value\":2},{\"label\":\"2016-01-04\",\"value\":0},{\"label\":\"2016-01-05\",\"value\":0},{\"label\":\"2016-01-06\",\"value\":2},{\"label\":\"2016-01-07\",\"value\":2}]},{\"summoner\":\"Sarénya\",\"courbeName\":\"defeats\",\"courbeSummoner\":[{\"label\":\"2016-01-01\",\"value\":1},{\"label\":\"2016-01-02\",\"value\":5},{\"label\":\"2016-01-03\",\"value\":1},{\"label\":\"2016-01-04\",\"value\":1},{\"label\":\"2016-01-05\",\"value\":1},{\"label\":\"2016-01-06\",\"value\":1},{\"label\":\"2016-01-07\",\"value\":1}]},{\"summoner\":\"Sobievsky\",\"courbeName\":\"victories\",\"courbeSummoner\":[{\"label\":\"2016-01-02\",\"value\":0}]},{\"summoner\":\"Sobievsky\",\"courbeName\":\"defeats\",\"courbeSummoner\":[{\"label\":\"2016-01-02\",\"value\":1}]}],\"indices\":[\"2016-01-01\",\"2016-01-02\",\"2016-01-03\",\"2016-01-04\",\"2016-01-05\",\"2016-01-06\",\"2016-01-07\"],\"date\":{\"from\":\"\",\"to\":\"\"},\"display\":{\"name\":\"null\",\"type\":\"line\"}}"))
		);
	}
	
	@ParameterizedTest()
	@MethodSource("getQueries")
	@DisplayName("returns the given request")
	void interpretsTheQuery(Request request) {
		process = new RequestProcess(request);
		assertThat(process.getRequest()).isEqualTo(request);
	}
	
	static Stream<Arguments> getQueries() {
		return Stream.of(
			Arguments.of(withoutFilters),
			Arguments.of(filterChampionTeam),
			Arguments.of(filterResultLane),
			Arguments.of(byYearWith2Summoners),
			Arguments.of(byTeamWith3Summoners),
			Arguments.of(byChampionWith3Summoners),
			Arguments.of(byDayWith2SummonersAnd2Display)
		);
	}
	
	private static Request parseQuery(String query) {
		Injector injector = GloeInjector.get();
    	ResourceSet resources = injector.getInstance(XtextResourceSet.class);
		
		Resource resource = resources.createResource(URI.createURI("dummy:/temporary.gloe"));
		InputStream in = new ByteArrayInputStream(query.getBytes());
		
		try {
			resource.load(in, resources.getLoadOptions());
			return (Request) resource.getContents().get(0);
		
		} catch (IOException e) {
			return null;
		}
	}

}
