package ice.master.loe;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.stream.JsonReader;

/**
 * Provides utility methods used by the tests
 * 
 * @author Emmanuel CHEBBI
 */
public interface TestUtils {

	/**
	 * Parses a JSON file.
	 * 
	 * @param filename
	 *            The name of the JSON file to parse.
	 *            Should be relative to {@code src/test/resources/}.
	 *            
	 * @return the corresponding {@link JsonObject}.
	 * 
	 * @throws FileNotFoundException if the file is not found
	 */
	default JsonObject getJsonObject(String filename) throws FileNotFoundException {
		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource(filename).getFile());
		JsonReader reader = new JsonReader(new FileReader(file));
		return new Gson().fromJson(reader, JsonObject.class);
	}

}
