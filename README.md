# League of Eevees — Back

## League of Eevees

This repository is part of the [League of Eevees (LoE)](https://gitlab.com/Eevee-IDM) project.

For detailed explanations about the project, please take a look at the **[wiki](https://gitlab.com/Eevee-IDM/league-of-eevees-wiki/wikis/home)**.

## Back-end Repository

### Objectives

This repository contains the server of LoE. This server defines three REST endpoints:
1. `gloe/`: the endpoint for user queries. Receives user requests, interprets them then send back the result (which is finally displayed as a diagram by the web interface).
2. `xtext-services/`: the endpoint for DSL-related services. Allows the text editor embedded within the web interface to benefit from autocompletion and syntactic verification.
3. `bookmarks/:` the endpoint for users' favorites. Provides a way to bookmark a request so that a user can re-use it easily without having to type it again.

> **Did you know ?** _Gloe_ is the name we gave to our request DSL.

### Architecture

The repository is organized as follows:
```
.
├───src
│   ├───main
│   │   ├───java
│   │   │   └───ice
│   │   │       └───master
│   │   │           └───loe
│   │   │               ├───repository
│   │   │               │   ├───elastic
│   │   │               │   └───rito
│   │   │               └───rest
│   │   │                   ├───cors
│   │   │                   ├───exceptions
│   │   │                   ├───bookmarks
│   │   │                   ├───gloe
│   │   │                   └───processing
│   └───test
│       ├───java
│       │   └───ice
│       │       └───master
│       │           └───loe
│       │               ├───repository
│       │               │   └───elastic
│       │               └───rest
│       │                   ├───bookmarks
│       │                   └───processing
```

The main packages are described in the following table:

| Packages                         | Content           | Endpoint |
| ------------------------------- |:-------------|:----------:|
| `ice.master.loe.repository`     | Classes used query LoL-related information, from either LoL API or our Elasticsearch database. |  |
| `ice.master.loe.rest.gloe`      | xText-related generated classes, copied from [business](https://gitlab.com/Eevee-IDM/league-of-eevees-business). | `xtext-services/` |
| `ice.master.loe.rest.processing`| Classes used to interpret user's request. | `gloe/` |
| `ice.master.loe.rest.bookmarks` | Classes used to manage user's bookmarks by querying our Elasticsearch database. | `bookmarks/` |

## Use the repository

### From a terminal

##### 1. Clone this repository:

> `git clone https://gitlab.com/Eevee-IDM/league-of-eevees-back`

##### 2. Build the project:

> `mvn verify`

##### 3. Run the server

> `java -jar target/loe-backend.jar`

> **Note**: by default, the server is launched on `http://0.0.0.0:5150/`. Another address can be passed as argument: `java -jar target/loe-backend.jar "http://localhost:4300"`

### From Eclipse IDE

#### Requirements

- The [M2Eclipse](https://www.eclipse.org/m2e/) plug-in must be available in Eclipse IDE. It provides Maven support.
- The [Project Lombok](https://projectlombok.org) must also be [installed](https://projectlombok.org/setup/eclipse), otherwise the project won't compile.

#### Import the project

1. Import the project in the workspace (_Import..._ > _Existing Maven Projects_)
2. Update Maven dependencies if needed (Right-click on the project > _Maven_ > _Update Project..._)
3. Run `ice.master.loe.rest.Main`

## Technologies used

The server has been written in Java. It relies on the following libraries:

| Libraries | Used to... |
|:----------: | :----- |
| [Grizzly](https://javaee.github.io/grizzly/) / [Jersey](https://jersey.github.io) | Define the REST web services. |
| [LoE (business)](https://gitlab.com/Eevee-IDM/league-of-eevees-business) | Parse user requests and create corresponding objects. |
| [GSON](https://github.com/google/gson) | Serialize and deserialize JSON. |

In order to speed up similar requests, we cache the result of computed requests within an Elasticsearch database. To this end, we use the following libraries:

| Libraries | Used to... |
|:----------: | :----- |
| [Elasticsearch Java API](https://www.elastic.co/guide/en/elasticsearch/client/java-api/6.2/index.html) | Low level interaction with the database. |
| [Jest](https://github.com/searchbox-io/Jest/tree/master/jest) | High level interaction with the database.| 

Finally, some libraries are used to the only purpose of easing our developments:

| Libraries | Used to... |
|:----------: | :----- |
| [Log4J](https://logging.apache.org/log4j/2.x/) | Log events. |
| [Lombok](https://projectlombok.org) | Reduce code verbosity.| 
