# Build project
mvn clean package

# Stop current docker
docker stop loe-backend

# Build new docker
docker build -t registry.gitlab.com/eevee-idm/league-of-eevees-back:latest .

# Run docker
docker run -d --rm --name loe-backend -p 137.74.154.94:80:5150 registry.gitlab.com/eevee-idm/league-of-eevees-back:latest

docker push registry.gitlab.com/eevee-idm/league-of-eevees-back:latest

# Delete all images with none tag
docker rmi $(docker images | grep "^<none>" | awk "{print $3}")

