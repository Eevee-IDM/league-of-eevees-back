FROM openjdk:8-jre

WORKDIR /opt/app

ADD target/loe-backend.jar ./

ENTRYPOINT ["java", "-jar", "loe-backend.jar"]